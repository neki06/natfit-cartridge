package refit.util;

import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import refit.config.REFITConfig;
import refit.message.REFITMessage;


public class REFITPayload {

	public  static final int HASH_SIZE = 16;
	private static final String HASH_FUNCTION = "MD5";

	public final byte[] payload;


	public REFITPayload(byte[] payload) {
		this.payload = payload;
	}

	public REFITPayload(REFITMessage message) {
		ByteBuffer buffer = message.getBuffer();
		if(buffer.isDirect()) {
			this.payload = new byte[buffer.capacity()];
			ByteBuffer copyBuffer = buffer.duplicate();
			copyBuffer.clear();
			copyBuffer.get(payload);
		} else {
			this.payload = buffer.array();
		}
	}
	

	public boolean matches(byte[] data) {
		return matches(payload, data);
	}
	
	@Override
	public boolean equals(Object object) {
		if(REFITConfig.ENABLE_DEBUG_CHECKS) {
			if(object == null) return false;
			if(!(object instanceof REFITPayload)) return false;
		}
		REFITPayload other = (REFITPayload) object;
		return matches(other.payload);
	}

	@Override
	public int hashCode() {
		return payload.length;
	}

	
	// #########################
	// # STATIC HELPER METHODS #
	// #########################
	
	private static final Map<Long, MessageDigest> digests = new HashMap<Long, MessageDigest>();
	

	public static MessageDigest getDigest() {
		MessageDigest digest = digests.get(Thread.currentThread().getId());
		if(digest == null) {
			try {
				digest = MessageDigest.getInstance(HASH_FUNCTION);
				digests.put(Thread.currentThread().getId(), digest);
			} catch(NoSuchAlgorithmException nsae) {
				nsae.printStackTrace();
			}
		}
		return digest;
	}

	public static byte[] createHash(byte[] data) {
		MessageDigest hashFunction = getDigest();
		hashFunction.update(data);
		return hashFunction.digest();
	}

	public static boolean matches(byte[] chunkA, byte[] chunkB) {
		// Simple checks first
		if(chunkA == null) return false;
		if(chunkB == null) return false;
		
		// Compare chunk lengths
		if(chunkA.length != chunkB.length) return false;
		
		// Compare chunk contents
		for(int i = 0; i < chunkA.length; i++) {
			if(chunkA[i] != chunkB[i]) return false;
		}
		return true;
	}

	public static String toString(byte[] data) {
		String s = "";
		for(byte b: data) s += String.format("%04d", b);
		return s;
	}
	
}
