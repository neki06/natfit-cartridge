package refit.util;


public interface REFITStatisticsListener {

	public void statisticsIntervalResult(int resultIndex, int eventCount, float eventValueAverage, long eventValueMin, long eventValueMax);
	public void statisticsProgressResult(int eventCount);
	public void statisticsOverallResult(int nrOfIntervals, int eventCount, float eventCountAverage, float eventValueAverage);
	
}
