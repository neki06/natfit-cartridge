package refit.replica.order;

import refit.config.REFITConfig;
import refit.message.REFITBatch;
import refit.message.REFITMessage;
import refit.message.REFITMessageType;
import refit.message.REFITRequest;
import refit.replica.REFITReplica;
import refit.replica.execution.REFITExecutionStage;
import refit.replica.execution.REFITOperation;
import refit.replica.execution.REFITOperation.REFITOperationType;
import refit.util.REFITMessageStatistics;


public abstract class REFITOrderProtocolInstance {

	protected final REFITReplica replica;
	protected final REFITOrderStageSlot slot;
	private   final REFITExecutionStage exectstage;
	
	public long instanceID;
	public int viewID;
	
	
	public REFITOrderProtocolInstance(REFITOrderStageSlot slot) {
		this.replica = slot.replica;
		this.slot = slot;
		this.messageStore = slot.messageStore;
		this.instanceID = -1L;
		this.viewID = -1;
		this.exectstage = replica.executionStages[ REFITConfig.ORDERSTAGE.getExecutionStageID( slot.orderStage.id ) ];
	}
	
	
	protected void updateView(int viewID, REFITOrderProtocol protocol) {
		this.viewID = viewID;
		slot.advanceView(viewID, protocol);
	}
	
	
	// ##############
	// # LIFE CYCLE #
	// ##############

	public boolean complete;
	
	
	public void init(long instanceID, int viewID) {
		this.instanceID = instanceID;
		this.viewID = viewID;
		this.complete = false;
	}
	
	public abstract void execute();
	
	public abstract REFITOrderProtocolInstance abort();

	protected void complete(REFITMessage result, REFITOrderProtocolInstance successorInstance, boolean finalInstance) {
		// Mark instance complete
		complete = true;
		
		// Nothing more to do if the instance did not produce a result
		if(result == null) {
			slot.instanceComplete(successorInstance, finalInstance);
			return;
		}
		
		// Handle result
		if(result != REFITRequest.NO_OP) {
			// Notify order stage
			slot.orderStage.messageStable(result.uid);
	
			// Prepare committed handle for delivery
			updateResult(result, instanceID);

			// Special treatment for batches
			if(REFITConfig.ENABLE_BATCHING && (result.type == REFITMessageType.BATCH)) {
				for(REFITMessage request: ((REFITBatch) result).requests) updateResult(request, instanceID);
			}
		}

		// Deliver stable result
		REFITOperation operation = new REFITOperation(REFITOperationType.REQUEST, instanceID, result);
		exectstage.insertMessage(operation);
		
		// Complete instance
		slot.instanceComplete(successorInstance, finalInstance);
	}
	
	protected void updateResult(REFITMessage result, long agreementSeqNr) {
		result.getContext().agreementSeqNr = agreementSeqNr;
		if(REFITConfig.COLLECT_MESSAGE_STATISTICS) REFITMessageStatistics.track(result, "DEL");
	}
	
	
	// ####################
	// # MESSAGE HANDLING #
	// ####################

	private final REFITOrderStageSlotMessageStore messageStore;
	
	
	protected REFITMessage fetchMessage(REFITMessageType type) {
		return fetchMessage(type, -1);
	}
	
	protected REFITMessage fetchMessage(REFITMessageType type, int viewID) {
		return messageStore.remove(type, viewID);
	}
	
	
	// #####################
	// # PROPOSAL HANDLING #
	// #####################

	public abstract boolean isProposer();

	
	protected REFITMessage fetchProposal() {
		return slot.orderStage.fetchProposal(instanceID);
	}

	
	// #############################
	// # ACCESS TO OTHER INSTANCES #
	// #############################

	protected REFITOrderProtocolInstance getProtocolInstance(long id) {
		REFITOrderStageSlot otherSlot = replica.getOrderStageForInstance(id).getSlot(id);
		return otherSlot.getProtocolInstance(id);
	}
	
}
