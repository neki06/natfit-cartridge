package refit.replica.order;

import java.nio.ByteBuffer;

import refit.message.REFITMessage;
import refit.message.REFITMessageType;
import refit.message.REFITUniqueID;


public abstract class REFITOrderProtocolMessage extends REFITMessage {

	public static final int ORDER_HEADER_SIZE = ((Long.SIZE + Integer.SIZE) >> 3);
	
	public final long instanceID;
	public final int viewID;

	
	protected REFITOrderProtocolMessage(long instanceID, int viewID, REFITUniqueID uid, short from) {
		super(REFITMessageType.ORDER, uid, from);
		this.instanceID = instanceID;
		this.viewID = viewID;
	}
	
	protected REFITOrderProtocolMessage(ByteBuffer buffer) {
		super(buffer);
		this.instanceID = buffer.getLong();
		this.viewID = buffer.getInt();
	}
	
	
	@Override
	public String toString() {
		return String.format("{%s|%d|%d|%d|%d|%d}", type, from, uid.nodeID, uid.seqNr, instanceID, viewID);
	}

	@Override
	protected void serialize(ByteBuffer buffer) {
		super.serialize(buffer);
		buffer.putLong(instanceID);
		buffer.putInt(viewID);
	}
	
	@Override
	protected int getHeaderSize() {
		return (super.getHeaderSize() + ORDER_HEADER_SIZE);
	}
	
	@Override
	protected int calculatePayloadSize() {
		return (super.calculatePayloadSize() + ORDER_HEADER_SIZE);
	}

	@Override
	protected REFITMessageType getPrimaryType(byte magic) {
		return REFITMessageType.ORDER;
	}
	
}
