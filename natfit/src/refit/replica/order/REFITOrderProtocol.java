package refit.replica.order;

import refit.config.REFITConfig;


public abstract class REFITOrderProtocol {

	protected static final boolean[] NO_REPLICA = new boolean[REFITConfig.TOTAL_NR_OF_REPLICAS];
	protected static final boolean[] ALL_REPLICAS = new boolean[REFITConfig.TOTAL_NR_OF_REPLICAS];
	static {
		for(int i = 0; i < ALL_REPLICAS.length; i++) ALL_REPLICAS[i] = true;
	}

	
	// ###########
	// # VIEW ID #
	// ###########
	
	protected int viewID;
	
	
	public void setViewID(int viewID) {
		this.viewID = viewID;
	}
	
	
	// ####################
	// # ABSTRACT METHODS #
	// ####################

	public abstract short getContactReplicaID(short localID);
	public abstract REFITOrderProtocolInstance createInstance(REFITOrderStageSlot slot);


	// ###################
	// # DEFAULT METHODS #
	// ###################

	public boolean[] getOrderRecipients() {
		return ALL_REPLICAS;
	}
	
	public boolean[] getUpdateRecipients() {
		return NO_REPLICA;
	}
	
	public boolean[] getCheckpointRecipients() {
		return ALL_REPLICAS;
	}
	
	public boolean useLightweightCheckpoints() {
		return false;
	}

	public boolean useEagerPanic() {
		return false;
	}
	
	
	// ##################
	// # HELPER METHODS #
	// ##################

	public boolean hasCheckpointRecipients() {
		boolean[] recipients = getCheckpointRecipients();
		for(boolean recipient: recipients) {
			if(recipient) return true;
		}
		return false;
	}
	
	public boolean hasUpdateRecipients() {
		boolean[] recipients = getUpdateRecipients();
		for(boolean recipient: recipients) {
			if(recipient) return true;
		}
		return false;
	}

}
