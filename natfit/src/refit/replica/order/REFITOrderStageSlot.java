package refit.replica.order;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

import refit.config.REFITConfig;
import refit.config.REFITLogger;
import refit.replica.REFITReplica;
import refit.replica.REFITWorkerStage;


public class REFITOrderStageSlot implements Comparable<REFITOrderStageSlot> {

	protected final REFITReplica    replica;
	protected final REFITOrderStage orderStage;
	protected final int             id;
	protected final int[]           wrkids;
	
	private REFITOrderProtocolInstance currentInstance;
	private REFITOrderProtocolInstance successorInstance;
	
	private REFITOrderProtocol currentProtocol;
	private long currentInstanceID;
	private int currentViewID;
	private int salt;
	
	
	public REFITOrderStageSlot(REFITReplica replica, REFITOrderStage orderStage, int id, REFITOrderProtocol initialProtocol) {
		// Initialize data structures
		this.replica = replica;
		this.orderStage = orderStage;
		this.id         = id;
		this.messageStore = new REFITOrderStageSlotMessageStore();
		this.instanceMessages = new HashMap<Long, Collection<REFITOrderProtocolMessage>>();
		this.currentProtocol = initialProtocol;
		this.currentInstanceID = -1L;
		this.currentViewID = 0;
		this.wrkids = REFITConfig.ORDERSTAGE.getWorkerIDs( orderStage.id );

		salt = hashCode();
	}

	public REFITWorkerStage getWorker(int salt)
	{
		return wrkids != null ? replica.workerStages[ wrkids[ Math.abs( salt % wrkids.length ) ] ] : null;
	}

	public REFITOrderStage getOrderStage()
	{
		return orderStage;
	}

	@Override
	public String toString() {
		return String.format( "ORDSL%02d%04d[%d-%d]", orderStage.id, id, currentInstanceID, currentViewID, currentViewID);
	}
	
	@Override
	public int compareTo(REFITOrderStageSlot slot) {
		return (int) (currentInstanceID - slot.currentInstanceID);
	}

	public REFITOrderProtocolInstance getProtocolInstance(long instanceID) {
		return (currentInstanceID == instanceID) ? currentInstance : null;
	}
	
	public boolean isProposer() {
		return currentInstance.isProposer();
	}
	
	@SuppressWarnings("unused")
    protected void advanceView(int viewID, REFITOrderProtocol protocol) {
		if(REFITConfig.ENABLE_DEBUG_CHECKS && (viewID < currentViewID)) REFITLogger.logDebug(this, "unexpected reset of view id " + currentViewID + " -> " + viewID);
		boolean notifyOrderStage = (currentViewID != viewID) || (currentProtocol != protocol);
		currentViewID = viewID;
		currentProtocol = protocol;
		if(notifyOrderStage)
			// TODO: Implement view-change notifications
			throw new UnsupportedOperationException();
//			replica.orderStage.learnViewChange(viewID, protocol);
	}

	
	// ######################
	// # LIFE-CYCLE METHODS #
	// ######################
	
	public boolean init(long instanceID) {
		// Update current protocol instance
		currentInstance = ((successorInstance != null) && (instanceID == successorInstance.instanceID)) ? successorInstance : currentProtocol.createInstance(this);
		currentInstanceID = instanceID;
		successorInstance = null;
		
		// Initialize protocol instance
		currentInstance.init(instanceID, currentViewID);

		// Garbage-collect buffered messages for skipped instances
		for(Iterator<Long> iterator = instanceMessages.keySet().iterator(); iterator.hasNext(); ) {
			if(iterator.next() < instanceID) iterator.remove();
		}

		// Prepare message store for the new instance and demand instance execution if there are any buffered messages
		Collection<REFITOrderProtocolMessage> currentInstanceMessages = instanceMessages.remove(instanceID);
		messageStore.init(currentInstanceMessages);
		return (currentInstanceMessages != null);
	}
	
	public void execute() {
		if(currentInstance.complete) return;
		currentInstance.execute();
	}
	
	public void abort() {
		currentInstance = currentInstance.abort();
		successorInstance = null;
	}

	public void instanceComplete(REFITOrderProtocolInstance successorInstance, boolean finalInstance) {
		// Store successor instance if the current was the final instance for the particular instance id
		if(finalInstance) {
			this.successorInstance = successorInstance;
			return;
		}
		
		// Switch to successor instance
		currentInstance = successorInstance;
		successorInstance = null;
		
		// Invoke new instance
		execute();
	}
	

	// ####################
	// # MESSAGE HANDLING #
	// ####################

	protected final REFITOrderStageSlotMessageStore messageStore;
	private final Map<Long, Collection<REFITOrderProtocolMessage>> instanceMessages;
	
	
	public boolean insertMessage(REFITOrderProtocolMessage message) {
		// Ignore messages for old instances and old views
		if(message.instanceID < currentInstanceID) return false;
		if(message.viewID < currentViewID) return false;
		
		// Buffer messages for future instances
		if(message.instanceID != currentInstanceID) {
			Collection<REFITOrderProtocolMessage> messages = instanceMessages.get(message.instanceID);
			if(messages == null) {
				messages = new LinkedList<REFITOrderProtocolMessage>();
				instanceMessages.put(message.instanceID, messages);
			}
			messages.add(message);
			return false;
		}
		
		// Ignore message for current instance if the instance is already complete
		if(currentInstance.complete) return false;

		// Store message for current instance
		messageStore.add(message);
		return true;
	}


	public int generateMessageSalt()
	{
		return salt++;
	}

}
