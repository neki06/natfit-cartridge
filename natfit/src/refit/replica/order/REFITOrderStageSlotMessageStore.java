package refit.replica.order;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

import refit.message.REFITMessageType;


public class REFITOrderStageSlotMessageStore {

	private final REFITMessageStoreBucket[] buckets;


	public REFITOrderStageSlotMessageStore() {
		this.buckets = new REFITMessageStoreBucket[REFITMessageType.values().length];
		for(int i = 0; i < buckets.length; i++) buckets[i] = new REFITMessageStoreBucket();
	}


	public void init(Collection<? extends REFITOrderProtocolMessage> messages) {
		for(REFITMessageStoreBucket bucket: buckets) bucket.clear();
		if(messages == null) return;
		for(REFITOrderProtocolMessage message: messages) add(message);
	}

	public void add(REFITOrderProtocolMessage message) {
		REFITMessageStoreBucket bucket = buckets[message.getTypeMagic()];
		bucket.add(message);
	}

	public REFITOrderProtocolMessage remove(REFITMessageType type, int viewID) {
		REFITMessageStoreBucket bucket = buckets[type.getMagic()];
		return bucket.removeOrderMessage(viewID);
	}


	// ##########
	// # BUCKET #
	// ##########

	private static class REFITMessageStoreBucket extends LinkedList<REFITOrderProtocolMessage> {

		public REFITOrderProtocolMessage removeOrderMessage(int viewID) {
			// Optimistic dequeue for the common situation that the first message in the queue fits
			REFITOrderProtocolMessage message = poll();
			if((viewID < 0) || (message == null) || (message.viewID == viewID)) return message;
			
			// List repair
			addFirst(message);
			
			// Pessimistic dequeue
			for(Iterator<REFITOrderProtocolMessage> iterator = iterator(); iterator.hasNext(); ) {
				message = iterator.next();
				if(message.viewID != viewID) continue;
				iterator.remove();
				return message;
			}
			return null;
		}
		
	}

}
