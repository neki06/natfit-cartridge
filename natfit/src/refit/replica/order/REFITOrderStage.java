package refit.replica.order;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;
import java.util.TreeSet;

import refit.config.REFITConfig;
import refit.config.REFITLogger;
import refit.message.REFITBatch;
import refit.message.REFITInstruction;
import refit.message.REFITInstruction.REFIITConfigurationNotification;
import refit.message.REFITInstruction.REFITProgressNotification;
import refit.message.REFITMessage;
import refit.message.REFITMessageAuthentication;
import refit.message.REFITMulticatMACVerificationInstruction;
import refit.message.REFITRequest;
import refit.message.REFITUniqueID;
import refit.replica.REFITReplica;
import refit.replica.REFITStage;
import refit.replica.checkpoint.REFITCheckpointStage;
import refit.replica.client.REFITClientStage;
import refit.replica.execution.REFITExecutionStage;
import refit.scheduler.REFITSchedulerTaskType;


public class REFITOrderStage extends REFITStage {

	public REFITOrderStage(int id, REFITReplica replica, REFITOrderProtocol initialProtocol) {
		// Initialize data structures
		super(REFITSchedulerTaskType.ORDER_STAGE, replica);
		this.id         = id;
		this.readySlots = new TreeSet<REFITOrderStageSlot>();
		this.proposals = new LinkedList<REFITRequest>();
		this.nextProposerID = id;
		this.publishedViewID = -1;
		this.auth = REFITMessageAuthentication.createForReplicaConnection( replica.id );
		
		// Create protocol-instance slots
		this.slots = new REFITOrderStageSlot[REFITConfig.MAXIMUM_ORDER_INSTANCES_IN_PROGRESS_PER_STAGE];
		this.windowStartID = -REFITConfig.MAXIMUM_ORDER_INSTANCES_IN_PROGRESS_TOTAL + id;
		for(int i = 0; i < slots.length; i++) slots[i] = new REFITOrderStageSlot(replica, this, i, initialProtocol);
	}

	
	@Override
	public String toString() {
		return String.format( "ODR%02d", id );
	}
	
	protected void handleMessage(REFITMessage message) {
		switch(message.type) {
		case ORDER:
			// Forward message to the corresponding slot
			REFITOrderProtocolMessage orderMessage = (REFITOrderProtocolMessage) message;
			REFITOrderStageSlot slot = slots[ getSlotIndex( orderMessage.instanceID ) ];

			REFITStage worker;

			if( message.isVerified()==null && REFITConfig.WORKERSTAGE.useForVerification() &&
					(worker = slot.getWorker( slot.generateMessageSalt() )) != null )
			{
				// TODO: Actually it would depend on the protocol instance for this message,
				//       which verification scheme has to be used for it.
				worker.insertMessage( new REFITMulticatMACVerificationInstruction( message, this, auth.getKey() ) );
			}
			else
			{
				boolean ready = slot.insertMessage(orderMessage);
				if(ready) readySlots.add(slot);
			}
			break;
		case INSTRUCTION:
			handleInstruction((REFITInstruction) message);
			break;
		default:
			handleProposal(message);
		}
	}

	private void handleInstruction(REFITInstruction instruction) {
		switch(instruction.instructionType) {
		case PROGRESS_NOTIFICATION:
			REFITProgressNotification notification = (REFITProgressNotification) instruction;
			moveWindow(notification.agreementSeqNr + 1);
			break;
		case PANIC_NOTIFICATION:
			REFITLogger.logEvent(this, "protocol abort");
			abortCurrentInstances();
			break;
		default:
			REFITLogger.logError(this, "drop instruction of unexpected type " + instruction.instructionType);
		}
	}


	public final REFITMessageAuthentication getAuth()
	{
		return auth;
	}

	
	// ###########################
	// # PROTOCOL-INSTANCE SLOTS #
	// ###########################
	
	public final int                   id;
	private   final REFITOrderStageSlot[] slots;
	private   long windowStartID;
	private final REFITMessageAuthentication auth;
	
	private int getSlotIndex(long instanceID)
	{
		return (int) ( (instanceID - id) / REFITConfig.ORDERSTAGE.getNumber() % slots.length );
	}

	private long getNextLocalInstance(long instanceID)
	{
		long base = instanceID - (instanceID % REFITConfig.ORDERSTAGE.getNumber()) + id;
		return base >= instanceID ? base : base + REFITConfig.ORDERSTAGE.getNumber();
	}

	public REFITOrderStageSlot getSlot(long instanceID) {
		// TODO: Why + slots.length? Is it possible that instanceID is lower than 0?
		throw new UnsupportedOperationException();
		// Would return slots[ getSlotIndex( instanceID ) ]; be sufficient?
//		return slots[(int) ((instanceID + slots.length) % slots.length)];
	}

	private void moveWindow(long newWindowStartID) {
		newWindowStartID = getNextLocalInstance( newWindowStartID );

		// Only move window forward
		if(newWindowStartID <= windowStartID) return;
		if(REFITLogger.LOG_ORDER) REFITLogger.logOrder(this, "move window from start id " + windowStartID + " to " + newWindowStartID);
		
		// Calculate which protocol instances to initialize
		long initInstancesStartID = Math.max(windowStartID + REFITConfig.MAXIMUM_ORDER_INSTANCES_IN_PROGRESS_TOTAL, newWindowStartID);
		long initInstancesEndID = newWindowStartID + REFITConfig.MAXIMUM_ORDER_INSTANCES_IN_PROGRESS_TOTAL;
		if(REFITLogger.LOG_ORDER) REFITLogger.logOrder(this, "initialize protocol instances from " + initInstancesStartID + " to " + (initInstancesEndID - 1));
		
		int  slotidx = getSlotIndex( initInstancesStartID );
		// Initialize new protocol instances
		for(long instanceID = initInstancesStartID; instanceID < initInstancesEndID; instanceID += REFITConfig.ORDERSTAGE.getNumber()) {
			REFITOrderStageSlot slot = slots[ slotidx++ % slots.length ];
			readySlots.remove(slot);
			boolean ready = slot.init(instanceID);
			if(ready) readySlots.add(slot);
		}
		
		// Update window information
		windowStartID = newWindowStartID;
		nextProposerID = Math.max(nextProposerID, newWindowStartID);
	}
	
	private void abortCurrentInstances() {
		// Abort slots in order
		if(REFITLogger.LOG_ORDER) REFITLogger.logOrder(this, "abort instances " + windowStartID + " to " + (windowStartID + REFITConfig.MAXIMUM_ORDER_INSTANCES_IN_PROGRESS_TOTAL - 1));

		int sslot = getSlotIndex( windowStartID );
		for(int slotidx = sslot; slotidx < (sslot + slots.length); slotidx++) {
			REFITOrderStageSlot slot = slots[ slotidx % slots.length ];
			slot.abort();
			readySlots.add(slot);
		}
	}

	
	// ################################
	// # PROTOCOL-INSTANCE INVOCATION #
	// ################################

	private final Set<REFITOrderStageSlot> readySlots;
	
	
	protected void stageComplete() {
		// Assign proposals to idle proposers
		assignProposals();
		
		// Invoke ready protocol instances in the order of their IDs (-> use of TreeSet)
		for(REFITOrderStageSlot readySlot: readySlots) readySlot.execute();
		
		// Reset ready slots
		readySlots.clear();
	}
	
	
	// ############################
	// # VIEW-CHANGE NOTIFICATION #
	// ############################
	
	private int publishedViewID;
	
	
	public void learnViewChange(int newViewID, REFITOrderProtocol protocol) {
		// Ignore view changes that have already been published
		if(newViewID <= publishedViewID) return;
		publishedViewID = newViewID;
		protocol.setViewID(newViewID);
		REFITLogger.logEvent(this, "switch to " + protocol.getClass().getSimpleName() + " (view " + newViewID + ")");
		
		// Reset next proposer id
		nextProposerID = windowStartID;
		
		// Distribute replica-role information
		if( id == 0 )
		{
			REFIITConfigurationNotification notification = new REFIITConfigurationNotification(protocol);

			for( REFITClientStage cs : replica.clientStages )
				cs.insertMessage(notification);

			for( REFITExecutionStage es : replica.executionStages )
				es.insertMessage(notification);

			for( REFITCheckpointStage ks : replica.checkpointStages )
				ks.insertMessage(notification);
		}
	}
	
	
	// #############
	// # PROPOSALS #
	// #############

	// TODO: REFITMessage for proposals, REFITRequests for batches
	private final Queue<REFITRequest> proposals;
	private long nextProposerID;
	
	
	private void handleProposal(REFITMessage proposal) {
		proposals.add((REFITRequest) proposal);
		
	}
	
	private void assignProposals() {
		// Return if there are no proposals to assign
		int nrOfProposals = proposals.size();
		if(nrOfProposals == 0) return;
		
		// Return if the minimal batch size has not yet been reached
		if(REFITConfig.ENABLE_BATCHING) {
			if(nrOfProposals < REFITConfig.MINIMUM_BATCH_SIZE) return;
		}
		
		// Find idle proposer instances
		int slotidx = getSlotIndex( nextProposerID );
		for(long instanceID = nextProposerID;
				(instanceID < (windowStartID + REFITConfig.MAXIMUM_ORDER_INSTANCES_IN_PROGRESS_TOTAL)) && (nrOfProposals > 0);
				instanceID += REFITConfig.ORDERSTAGE.getNumber()) {
			// Ignore slot if its current instance is not a proposer
			REFITOrderStageSlot slot = slots[ slotidx++ % slots.length ];
			if(!slot.isProposer()) continue;
			
			// Mark slot as ready
			readySlots.add(slot);
			
			// Update proposer selection information
			nextProposerID = instanceID + REFITConfig.ORDERSTAGE.getNumber();
			nrOfProposals = REFITConfig.ENABLE_BATCHING ? 0 : nrOfProposals - 1;
		}
		
		// Update proposer selection information if not enough proposers have been available
		if(nrOfProposals > 0) nextProposerID = windowStartID + REFITConfig.MAXIMUM_ORDER_INSTANCES_IN_PROGRESS_TOTAL;
	}

	public REFITMessage fetchProposal(long instanceID) {
		// Check whether there are messages to be proposed
		if(proposals.isEmpty()) return null;
		
		// Handle single proposals
		if(!REFITConfig.ENABLE_BATCHING || (proposals.size() == 1)) {
			return proposals.poll();
		}

		// Handle batch proposals
		Collection<REFITRequest> proposalBatch = new LinkedList<REFITRequest>();
		for(int i = 0; i < REFITConfig.MAXIMUM_BATCH_SIZE; i++) {
			REFITRequest proposal = proposals.poll();
			if(proposal == null) break;
			proposalBatch.add(proposal);
		}
		
		REFITBatch batch = new REFITBatch(instanceID, replica.id, proposalBatch);
		batch.serializeMessage(0);
		return batch;
	}
	
	public void messageStable(REFITUniqueID uid) {
		for(Iterator<REFITRequest> iterator = proposals.iterator(); iterator.hasNext(); ) {
			REFITRequest proposal = iterator.next();
			if(!proposal.uid.equals(uid)) continue;
			iterator.remove();
			break;
		}
	}
	
}
