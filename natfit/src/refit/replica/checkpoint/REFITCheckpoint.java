package refit.replica.checkpoint;

import java.nio.ByteBuffer;

import refit.config.REFITConfig;
import refit.message.REFITMessage;
import refit.message.REFITMessageType;
import refit.message.REFITUniqueID;
import refit.util.REFITPayload;


public class REFITCheckpoint extends REFITMessage {

	public final boolean isFullCheckpoint;
	public final long agreementSeqNr;
	public final byte[] payload;
	public final long[] nodeProgresses;

	
	public REFITCheckpoint(short from, long agreementSeqNr, byte[] payload, long[] nodeProgresses, boolean isFullCheckpoint) {
		this(new REFITUniqueID((short) -1, agreementSeqNr), from, payload, nodeProgresses, isFullCheckpoint);
	}
	
	private REFITCheckpoint(REFITUniqueID uid, short from, byte[] payload, long[] nodeProgresses, boolean isFullCheckpoint) {
		super(REFITMessageType.CHECKPOINT, uid, from);
		this.agreementSeqNr = uid.seqNr;
		this.payload = payload;
		this.nodeProgresses = new long[REFITConfig.ADDRESSES.length];
		for(int i = 0; i < this.nodeProgresses.length; i++) this.nodeProgresses[i] = nodeProgresses[i]; 
		this.isFullCheckpoint = isFullCheckpoint;
		if(!isFullCheckpoint) setHash(payload);
	}

	public REFITCheckpoint(ByteBuffer buffer) {
		super(buffer);
		this.isFullCheckpoint = (buffer.get() == 1);
		this.agreementSeqNr = uid.seqNr;
		this.payload = new byte[buffer.getInt()];
		buffer.get(payload);
		this.nodeProgresses = new long[REFITConfig.ADDRESSES.length];
		for(int i = 0; i < nodeProgresses.length; i++) nodeProgresses[i] = buffer.getLong();
		if(!isFullCheckpoint) setHash(payload);
	}

	
	@Override
	public String toString() {
		return String.format("{%s|%d|%d|%s}", type, from, uid.seqNr, payload);
	}
	
	@Override
	protected byte[] calculateHash() {
		return REFITPayload.createHash(payload);
	}
	
	@Override
	protected void serialize(ByteBuffer buffer) {
		super.serialize(buffer);
		buffer.put((byte) (isFullCheckpoint ? 1 : 0));
		buffer.putInt(payload.length);
		buffer.put(payload, 0, payload.length);
		for(long clientProgress: nodeProgresses) buffer.putLong(clientProgress);
	}
	
	@Override
	protected int calculatePayloadSize() {
		return (super.calculatePayloadSize() + ((Byte.SIZE + Integer.SIZE) >> 3) + payload.length + ((Long.SIZE >> 3) * nodeProgresses.length));
	}

}
