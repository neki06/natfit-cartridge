package refit.replica.client;

import java.util.HashMap;
import java.util.Map;

import refit.config.REFITConfig;
import refit.config.REFITLogger;
import refit.message.REFITInstruction.REFITPanicNotification;
import refit.message.REFITRequest;
import refit.message.REFITUniqueID;
import refit.replica.REFITReplica;
import refit.replica.checkpoint.REFITCheckpointStage;
import refit.replica.order.REFITOrderStage;


public class REFITPanicManager {

	private final REFITReplica replica;
	private final Map<Short, REFITPanicCacheEntry> cache;
	
	private long latestPanicNotification;
	private long panicInterval;
	
	
	public REFITPanicManager(REFITReplica replica) {
		this.replica = replica;
		this.cache = new HashMap<Short, REFITPanicCacheEntry>(REFITConfig.TOTAL_NR_OF_CLIENTS);
		clear();
	}

	
	@Override
	public String toString() {
		return "PNCCH";
	}


	public void clear() {
		cache.clear();
		latestPanicNotification = Long.MIN_VALUE;
		panicInterval = REFITConfig.REPLICA_PANIC_INTERVAL;
	}
	
	public void handlePanic(REFITRequest request, short contactReplicaID) {
		// Get entry for client
		boolean knownPanic = false;
		REFITPanicCacheEntry entry = cache.get(request.uid.nodeID);
		if(entry == null) {
			// Create new entry
			entry = new REFITPanicCacheEntry();
			cache.put(request.uid.nodeID, entry);
		} else {
			// Ignore old panics
			if(request.uid.seqNr < entry.uid.seqNr) return;

			// Check whether a panic for this UID has already been received
			if(request.uid.seqNr == entry.uid.seqNr) knownPanic = true;
		}
		
		// Handle first panic for this UID
		if(!knownPanic) {
			// Update entry
			entry.uid = request.uid;
			entry.timestamp = System.currentTimeMillis();
			
			// Forward request to contact replica
			if(replica.id != contactReplicaID) replica.replicaUnicast(request, contactReplicaID, (int) request.uid.seqNr);
			return;
		}

		// Do not react on frequent panics
		if((entry.timestamp + REFITConfig.REPLICA_REQUEST_TIMEOUT) > System.currentTimeMillis()) return;

		// Declare panic event
		panicEvent();
	}
	
	
	private void panicEvent() {
		// Control the rates in which panic notifications are sent
		if(System.currentTimeMillis() < (latestPanicNotification + panicInterval)) return;
		
		// Forward panic to order stage
		REFITLogger.logEvent(this, "send panic notification");
		REFITPanicNotification panicNotification = new REFITPanicNotification();

		for( REFITOrderStage os : replica.orderStages )
			os.insertMessage(panicNotification);

		for( REFITCheckpointStage cs : replica.checkpointStages )
			cs.insertMessage(panicNotification);
		
		// Update panic information
		latestPanicNotification = System.currentTimeMillis();
		panicInterval *= 2;
	}
	
	
	// ###############
	// # CACHE ENTRY #
	// ###############
	
	private static class REFITPanicCacheEntry {
		
		public REFITUniqueID uid;
		public long timestamp;
		
	}

}
