package refit.replica.client;

import javax.crypto.SecretKey;

import refit.config.REFITConfig;
import refit.message.REFITInstruction.REFITWorkNotification;
import refit.message.REFITMessageAuthentication;
import refit.message.REFITReply;
import refit.replica.REFITReplica;

public class REFITReplyInstruction extends REFITWorkNotification
{
	private final REFITReplica replica;
	private final REFITReply   reply;
	private final SecretKey    key;

	public REFITReplyInstruction(REFITReplica replica, REFITReply reply, SecretKey key)
	{
		this.replica = replica;
		this.reply   = reply;
		this.key     = key;
	}


	@Override
	public void execute(REFITMessageAuthentication auth)
	{
		boolean serialized = reply.serializeMessage(REFITConfig.HMAC_UNICAST_SIZE);

		if(serialized)
		{
			auth.initKey( key );
			auth.appendUnicastMAC(reply.uid.nodeID, reply);
		}

		replica.sendMessageToClient(reply, reply.uid.nodeID);
	}
}
