package refit.replica.client;

import refit.config.REFITConfig;
import refit.config.REFITLogger;
import refit.message.REFITInstruction;
import refit.message.REFITInstruction.REFIITConfigurationNotification;
import refit.message.REFITMessage;
import refit.message.REFITMessageAuthentication;
import refit.message.REFITMulticatMACVerificationInstruction;
import refit.message.REFITReply;
import refit.message.REFITRequest;
import refit.replica.REFITReplica;
import refit.replica.REFITStage;
import refit.replica.order.REFITOrderProtocol;
import refit.scheduler.REFITSchedulerTaskType;
import refit.util.REFITMessageStatistics;


public class REFITClientStage extends REFITStage {

	public  final int id;
	private final REFITReplyCache replyCache;
	private final REFITPanicManager panicManager;
	private short contactReplicaID;
	private int   nextOrderStage = 0;
	private boolean sendReplies;
	private boolean eagerPanic;
	private final int[] orderStageIDs;
	private final int[] wrkIDs;
	private int         salt;

	private REFITMessageAuthentication[] auths;


	public REFITClientStage(int id, REFITReplica replica) {
		super(REFITSchedulerTaskType.CLIENT_STAGE, replica);
		this.id = id;
		this.replyCache = new REFITReplyCache();
		this.panicManager = new REFITPanicManager(replica);
		this.contactReplicaID = -1;
		this.sendReplies = true;
		this.eagerPanic = false;
		this.orderStageIDs = REFITConfig.CLIENTSTAGE.getOrderStageIDs( id );
		this.wrkIDs = REFITConfig.CLIENTSTAGE.getWorkerIDs( id );

		if( REFITConfig.HMAC_DIGESTONLY )
			auths = new REFITMessageAuthentication[] { REFITMessageAuthentication.createForReplicaConnection( replica.id ) };
		else
		{
			auths = new REFITMessageAuthentication[ REFITConfig.TOTAL_NR_OF_CLIENTS ];
			for( short i=0; i<auths.length; i++ )
				auths[i] = REFITMessageAuthentication.createForClientConnection( replica.id, (short) (i+REFITConfig.TOTAL_NR_OF_REPLICAS), replica.id );
		}

		salt = hashCode();
	}


	@Override
	public String toString() {
		return String.format( "CLI%02d", id );
	}


	@Override
	protected void handleMessage(REFITMessage message) {
		switch(message.type) {
		case REQUEST:
			handleRequest((REFITRequest) message);
			break;
		case REPLY:
			handleReply((REFITReply) message);
			break;
		case INSTRUCTION:
			handleInstruction((REFITInstruction) message);
			break;
		default:
			REFITLogger.logError(this, "drop message of unexpected type " + message.type);
		}
	}


	private final REFITMessageAuthentication getAuth(short clientid)
	{
		return auths.length==1 ? auths[0] : auths[ clientid-REFITConfig.TOTAL_NR_OF_REPLICAS ];
	}

	private void handleRequest(REFITRequest request) {
		// Help the client to determine its initial sequence number
//		if(request.uid.seqNr < 0) {
//			handleInitialRequest(request);
//			return;
//		}

		// Ignore old and faulty requests
		if(replyCache.isOldUID(request.uid)) return;

		if( wrkIDs != null && request.isVerified() == null && REFITConfig.WORKERSTAGE.useForVerification() )
		{
			deliverToWorker( new REFITMulticatMACVerificationInstruction( request, this, getAuth(request.from).getKey() ) );
			return;
		}

		if(!getAuth(request.from).verifyMulticastMAC(request)) return;

		// Query reply cache
		REFITReply reply = replyCache.getReply(request.uid);
		if(reply != null) {
			sendReply(reply);
			if(!eagerPanic) return;
		}

		// Handle panics
		if(request.isPanic) panicManager.handlePanic(request, contactReplicaID);

		// Ignore duplicate non-panic requests
		boolean success = replyCache.registerUID(request.uid);
		if(!success && !request.isPanic) return;

		// Forward request to local order stage
		replica.orderStages[ orderStageIDs[ nextOrderStage ] ].insertMessage(request);

		nextOrderStage = ++nextOrderStage % orderStageIDs.length;
	}

	private void handleReply(REFITReply reply) {
		// Store reply in cache
		if(REFITConfig.COLLECT_MESSAGE_STATISTICS) REFITMessageStatistics.showTrace(reply.context.statistics);
		replyCache.storeReply(reply);
		if(!sendReplies) return;

		// Create hash reply
		if(REFITConfig.USE_HASHED_REPLIES) {
			if(reply.sendHashReply) reply = new REFITReply(reply.uid, reply.from, false, reply.getPayloadHash());
		}

		// Send reply to client
		sendReply(reply);
	}

	private void sendReply(REFITReply reply) {
		reply.setContactReplicaID(contactReplicaID);

		if( wrkIDs != null )
			deliverToWorker( new REFITReplyInstruction( replica, reply, getAuth(reply.uid.nodeID).getKey() ) );
		else
		{
			boolean serialized = reply.serializeMessage(REFITConfig.HMAC_UNICAST_SIZE);
			if(serialized) getAuth(reply.uid.nodeID).appendUnicastMAC(reply.uid.nodeID, reply);
			replica.sendMessageToClient(reply, reply.uid.nodeID);
		}
	}

	private void deliverToWorker(REFITMessage msg)
	{
		replica.workerStages[ wrkIDs[ Math.abs( salt-- % wrkIDs.length ) ] ].insertMessage( msg );
	}

	private void handleInstruction(REFITInstruction instruction) {
		switch(instruction.instructionType) {
		case CONFIGURATION_NOTIFICATION:
			REFIITConfigurationNotification notification = (REFIITConfigurationNotification) instruction;
			REFITOrderProtocol protocol = notification.protocol;
			contactReplicaID = protocol.getContactReplicaID(replica.id);
			sendReplies = protocol.getOrderRecipients()[replica.id];
			eagerPanic = protocol.useEagerPanic();
			REFITLogger.logEvent(this, contactReplicaID + " is now the contact replica (send replies: " + sendReplies + ", eager panic: " + eagerPanic + ")");
			panicManager.clear();
			break;
		default:
			REFITLogger.logError(this, "drop instruction of unexpected type " + instruction.instructionType);
		}
	}


	// #################
	// # SPECIAL CASES #
	// #################

// TODO
//	private void handleInitialRequest(REFITRequest request) {
//		// Create initial reply containing the latest sequence number known at this replica
//		long latestKnownSeqNr = replyCache.getProgress(request.uid.nodeID);
//		byte[] initPayload = ByteBuffer.allocate(Long.SIZE >> 3).putLong(latestKnownSeqNr).array();
//		REFITReply initReply = new REFITReply(request.uid, replica.id, (short) -1, true, initPayload);
//
//		// Append MAC
//		initReply.serializeMessage(REFITMessageAuthentication.UNICAST_MAC_SIZE);
//		REFITMessageAuthentication.instance.appendUnicastMAC(initReply.uid.nodeID, initReply);
//
//		// Send initial reply to client
//		replica.sendMessageToClient(initReply, initReply.uid.nodeID);
//	}

}
