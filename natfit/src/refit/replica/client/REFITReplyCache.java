package refit.replica.client;

import java.util.HashMap;
import java.util.Map;

import refit.config.REFITConfig;
import refit.message.REFITReply;
import refit.message.REFITUniqueID;


public class REFITReplyCache {

	private final Map<Short, REFITClientRequestReplyCacheEntry> cache;

	
	public REFITReplyCache() {
		this.cache = new HashMap<Short, REFITClientRequestReplyCacheEntry>(REFITConfig.TOTAL_NR_OF_CLIENTS);
	}

	
	@Override
	public String toString() {
		return "RPYCH";
	}
	
	
	public boolean isOldUID(REFITUniqueID uid) {
		REFITClientRequestReplyCacheEntry entry = cache.get(uid.nodeID);
		if(entry == null) return false;
		return (uid.seqNr < entry.uid.seqNr);
	}

	public boolean registerUID(REFITUniqueID uid) {
		// Get entry for client
		REFITClientRequestReplyCacheEntry entry = cache.get(uid.nodeID);
		if(entry == null) {
			// Create new entry
			entry = new REFITClientRequestReplyCacheEntry();
			cache.put(uid.nodeID, entry);
		} else {
			// Make sure that only new UIDs are stored
			if(uid.seqNr <= entry.uid.seqNr) return false;
		}

		// Update entry
		entry.uid = uid;
		entry.reply = null;
		return true;
	}
	
	public void storeReply(REFITReply reply) {
		// Get entry for client
		REFITClientRequestReplyCacheEntry entry = cache.get(reply.uid.nodeID);
		if(entry == null) {
			// Create new entry
			entry = new REFITClientRequestReplyCacheEntry();
			cache.put(reply.uid.nodeID, entry);
		} else {
			// Make sure that only new replies are stored
			if(reply.uid.seqNr < entry.uid.seqNr) return;
		}
		
		// Store reply
		entry.uid = reply.uid;
		entry.reply = reply;
	}

	public REFITReply getReply(REFITUniqueID uid) {
		REFITClientRequestReplyCacheEntry entry = cache.get(uid.nodeID);
		if(entry == null) return null;
		if(uid.seqNr != entry.uid.seqNr) return null;
		return entry.reply;
	}

	
	// ###############
	// # CACHE ENTRY #
	// ###############
	
	private static class REFITClientRequestReplyCacheEntry {
		
		public REFITUniqueID uid;
		public REFITReply reply;
		
	}

}
