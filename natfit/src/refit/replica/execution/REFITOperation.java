package refit.replica.execution;

import refit.config.REFITConfig;
import refit.message.REFITMessage;
import refit.message.REFITMessageAuthentication;
import refit.message.REFITMessageType;
import refit.message.REFITUniqueID;
import refit.util.REFITPayload;


public class REFITOperation extends REFITMessage {

	public enum REFITOperationType {
		REQUEST,
		CHECKPOINT
	}
	
	
	public final REFITOperationType operationType;
	public final long agreementSeqNr;
	public final REFITPayload messageHash;
	public REFITMessage message;
	
	
	private REFITOperation(REFITOperationType operationType, long agreementSeqNr, REFITUniqueID uid, REFITPayload messageHash, REFITMessage message) {
		super(REFITMessageType.OPERATION, uid, (short) -1);
		this.operationType = operationType;
		this.agreementSeqNr = agreementSeqNr;
		this.messageHash = messageHash;
		this.message = message;
	}
	
	public REFITOperation(REFITOperationType operationType, long agreementSeqNr, REFITUniqueID uid, REFITPayload messageHash) {
		this(operationType, agreementSeqNr, uid, messageHash, null);
	}
	
	public REFITOperation(REFITOperationType operationType, long agreementSeqNr, REFITMessage message) {
		this(operationType, agreementSeqNr, message.uid, null, message);
	}

	
	public boolean setFullMessage(REFITMessage message, REFITMessageAuthentication auth) {
		if(message == null) return false;
		if(this.message != null) return false;
		if(!uid.equals(message.uid)) return false;
		if(!(REFITConfig.ENABLE_BATCHING && (message.type == REFITMessageType.BATCH)) && !messageHash.equals(message.getHash())) return false;
		if(operationType == REFITOperationType.CHECKPOINT) {
			if(!auth.verifySignature(message)) return false;
		} else {
			if(!auth.verifyMulticastMAC(message)) return false;
		}
		this.message = message;
		message.getContext().agreementSeqNr = agreementSeqNr;
		return true;
	}
	
	public boolean isExecutable() {
		return (message != null);
	}

	@Override
	public String toString() {
		return "{" + operationType + "|" + agreementSeqNr + "|" + uid + "|" + isExecutable() + "}";
	}
	
}
