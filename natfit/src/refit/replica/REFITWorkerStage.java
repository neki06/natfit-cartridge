package refit.replica;

import refit.config.REFITLogger;
import refit.message.REFITInstruction;
import refit.message.REFITInstruction.REFITWorkNotification;
import refit.message.REFITMessage;
import refit.message.REFITMessageAuthentication;
import refit.scheduler.REFITSchedulerTaskType;

public class REFITWorkerStage extends REFITStage
{
	public final int id;
	private final REFITMessageAuthentication auth;

	public REFITWorkerStage(int id, REFITReplica replica)
    {
	    super( REFITSchedulerTaskType.WORKER_STAGE, replica );

	    this.id = id;
	    this.auth = REFITMessageAuthentication.createForReplicaConnection( replica.id );
    }

	@Override
	public String toString()
	{
		return String.format( "WRK%02d", id );
	}

	@Override
	protected void handleMessage(REFITMessage message)
	{
		switch(message.type)
		{
		case INSTRUCTION:
			handleInstruction( (REFITInstruction) message );
			break;
		default:
			REFITLogger.logError(this, "drop message of unexpected type " + message.type);
		}
	}

	private void handleInstruction(REFITInstruction instruction)
	{
		switch(instruction.instructionType)
		{
		case WORK_NOTIFICATION:
			REFITWorkNotification wrknot = (REFITWorkNotification) instruction;
			wrknot.execute( auth );
			break;
	default:
			REFITLogger.logError(this, "drop instruction of unexpected type " + instruction.instructionType);
		}
	}
}
