package refit.smart;

import java.lang.reflect.Constructor;

import refit.client.REFITBenchmarkBase;
import refit.client.REFITBenchmarkClient;
import refit.client.REFITClientProxy;
import refit.config.REFITLogger;
import bftsmart.tom.ServiceProxy;

public class REFITSmartBenchmark extends REFITBenchmarkBase
{
	private final String cfgdir;

	public REFITSmartBenchmark(short id, long timelogresol, String cfgdir, String resultpath)
    {
	    super( id, timelogresol, resultpath );

	    this.cfgdir = cfgdir;
    }

	private static class SmartClientProxy implements REFITClientProxy
	{
		private final ServiceProxy proxy;

		public SmartClientProxy(int id, String cfgdir)
		{
			proxy = new ServiceProxy( id, cfgdir );
		}

		@Override
        public byte[] invoke(byte[] request) throws Exception
        {
			return proxy.invokeOrdered( request );
        }
	}


	public void runBenchmark(int clientidbase, int clientcnt, long duration, Class<?> benchmarkType)
	{
		REFITBenchmarkClient[] clients = new REFITBenchmarkClient[ clientcnt ];
		for(int i = 0; i < clientcnt; i++ ) {
			try {
				REFITClientProxy proxy = new SmartClientProxy( i + clientidbase, cfgdir );
				// Create client
				@SuppressWarnings("unchecked")
                Constructor<? extends REFITBenchmarkClient> benchmarkConstructor = (Constructor<? extends REFITBenchmarkClient>) benchmarkType.getConstructor(new Class[] { short.class, REFITClientProxy.class });
				clients[i] = benchmarkConstructor.newInstance(new Object[] { (short) (clientidbase + i), proxy });
			} catch(Exception e) {
				e.printStackTrace();
				System.exit(1);
			}
		}

		runBenchmark( duration, clients );
	}


	public static void main(String[] args) throws Exception
	{
		short    id           = Short.parseShort( args[0] );
		String   cfgdir       = args[1];
		String   resultpath   = args[2];
		int      clientidbase = Integer.parseInt(args[3]);
		int      clientcnt    = Integer.parseInt(args[4]);
		long     duration     = Integer.parseInt(args[5]) * 1000;
		Class<?> benchtype    = Class.forName(args[6]);

		REFITLogger.logInfo( "CONFIG", "ID: " + id );
		REFITLogger.logInfo( "CONFIG", "ConfigDir: " + cfgdir );
		REFITLogger.logInfo( "CONFIG", "ResultPath: " + resultpath );
		REFITLogger.logInfo( "CONFIG", "ClientIDBase: " + clientidbase );
		REFITLogger.logInfo( "CONFIG", "ClientCnt: " + clientcnt );
		REFITLogger.logInfo( "CONFIG", "Duration: " + duration );

		REFITSmartBenchmark benchmark = new REFITSmartBenchmark( id, 10, cfgdir, resultpath );
		benchmark.runBenchmark( clientidbase, clientcnt, duration, benchtype );
	}

}
