package refit.smart;

import java.nio.ByteBuffer;

import refit.communication.REFITMessageFormat;
import refit.message.REFITMessage;


public class REFITSmartMessageFormat extends REFITMessageFormat
{
	@Override
	public ByteBuffer getSendBuffer(ByteBuffer msgbuf)
	{
		ByteBuffer out = ByteBuffer.allocate( msgbuf.remaining()+5 );
		tryWriteMessage( out, msgbuf );
		out.rewind();
		return out;
	}


	@Override
	public boolean tryWriteMessage(ByteBuffer out, ByteBuffer msgbuf)
	{
		if( out.remaining()<msgbuf.remaining()+5 )
			return false;
		else
		{
	        out.putInt( 1 + msgbuf.remaining() );
	        out.put( (byte) 0 );
			out.put( msgbuf );

			return true;
		}
	}


	@Override
	public ByteBuffer tryReadMessage(ByteBuffer in)
	{
		if( in.remaining()<(Integer.SIZE>>3) )
			return null;

		int msgsize = in.getInt( in.position() );

		if( in.remaining()<msgsize+4 )
			return null;

		in.getInt();
		in.get();

		ByteBuffer msgbuf = ByteBuffer.allocate( msgsize-1 );
		in.get( msgbuf.array() );

		return msgbuf;
	}


	@Override
	public REFITMessage deserializeMessage(ByteBuffer msgbuf)
	{
		return REFITSmartReply.readFrom( msgbuf );
	}
}
