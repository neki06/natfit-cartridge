package refit.smart;

import refit.application.counter.REFITCounterServer;
import bftsmart.tom.MessageContext;
import bftsmart.tom.ServiceReplica;
import bftsmart.tom.server.defaultservices.DefaultRecoverable;


public class REFITSmartReplica extends DefaultRecoverable
{
	private final int                id;
	private final String             cfgdir;
	private final REFITCounterServer app;


	public REFITSmartReplica(int id, String cfgdir, REFITCounterServer app)
	{
		this.id     = id;
		this.cfgdir = cfgdir;
		this.app    = app;
	}


	@Override
    public byte[] executeUnordered(byte[] command, MessageContext msgCtx)
    {
		throw new UnsupportedOperationException();
    }


	@Override
    public byte[][] appExecuteBatch(byte[][] commands, MessageContext[] msgCtxs)
    {
		byte[][] replies = new byte[ commands.length ][];

		for( int i=0; i<commands.length; i++ )
			replies[i] = app.processRequest( msgCtxs[i].getSender(), commands[i], msgCtxs[i].getConsensusId() );

	    return replies;
    }


	@Override
    public byte[] getSnapshot()
    {
		return app.createCheckpoint();
    }


	@Override
    public void installSnapshot(byte[] state)
    {
		app.applyCheckpoint( state );
    }


	public void start()
	{
		new ServiceReplica( id, cfgdir, this, this );
	}


	public static void main(String[] args)
	{
		int    id           = Integer.parseInt( args[0] );
		String cfgdir       = args[1];
		int    clientcnt    = Integer.parseInt( args[2] );
		int    clientidbase = Integer.parseInt( args[3] );
		int    replysize    = Integer.parseInt( args[4] );

		REFITCounterServer app     = new REFITCounterServer( 0, 1, clientidbase, clientcnt, replysize );
		REFITSmartReplica  replica = new REFITSmartReplica( id, cfgdir, app );

		replica.start();
	}
}