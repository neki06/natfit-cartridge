package refit.smart;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

import refit.message.REFITReply;
import refit.message.REFITUniqueID;
import bftsmart.tom.core.messages.TOMMessage;

public class REFITSmartReply extends REFITReply
{
	private final TOMMessage tommsg;

	public REFITSmartReply(TOMMessage tommsg)
	{
		super( new REFITUniqueID( (short) -1, tommsg.getSequence() ),
				(short) tommsg.getSender(), true, tommsg.getContent(), null );

		this.tommsg = tommsg;
	}


	public static REFITSmartReply readFrom(ByteBuffer buf)
	{
		TOMMessage tommsg = new TOMMessage();

		int  size = buf.remaining();

		ByteArrayInputStream bais = new ByteArrayInputStream( buf.array(), buf.position(), size );
		DataInputStream      dis  = new DataInputStream(bais);
		try
		{
			tommsg.rExternal(dis);

			REFITSmartReply msg = new REFITSmartReply( tommsg );
			msg.setSerialized( size, dis.available(), buf );

			return msg;
		}
		catch( ClassNotFoundException | IOException e )
		{
			throw new IllegalStateException( e );
		}
	}

	public TOMMessage getTOMMessage()
	{
		return tommsg;
	}
}
