package refit.config;

public class REFITSchedulerConfig
{
	private final String name;
	private final int[]  affinity;

	public REFITSchedulerConfig(String name, int[] affinity)
	{
		this.name     = name;
		this.affinity = affinity;
	}

	public String getName()
	{
		return name;
	}

	public int[] getAffinity()
	{
		return affinity;
	}
}

