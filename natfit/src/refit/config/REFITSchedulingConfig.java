package refit.config;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import refit.scheduler.REFITSchedulerTaskType;

public class REFITSchedulingConfig
{
	private final int[] process_affinity;
	private final int[] scheduler_affinity;

	private final REFITSchedulerConfig[]             schedulers;
	private final Map<REFITSchedulerTaskType, int[]> task_assignment  = new HashMap<>();
	private final Map<REFITSchedulerTaskType, int[]> group_assignment = new HashMap<>();


	private REFITSchedulingConfig(Properties props, String group, String defname,
			Collection<REFITSchedulerTaskType> tasks)
	{
		process_affinity   = REFITConfig.getIntArrayProp( props, group + ".affinity", null );
		scheduler_affinity = REFITConfig.getIntArrayProp( props, "schedulers." + group + ".affinity", null );

		int initno = REFITConfig.getIntProp( props, "schedulers." + group + ".init_number", 1 );

		List<String> addscheds = new LinkedList<>();

		for( REFITSchedulerTaskType tt : tasks )
		{
			int[] g = REFITConfig.getIntArrayProp( props, "schedulers.task." + tt.toString().toLowerCase() + ".group", null );

			if( g != null )
				group_assignment.put( tt, g );
			else
			{
				int[] s = REFITConfig.getIntArrayProp( props, "schedulers.task." + tt.toString().toLowerCase(), null );

				if( g == null && s == null )
				{
					int nt = getNumberOfTasks( tt );

					s = new int[ nt ];

					for( int i = 0; i < nt; i++ )
					{
						s[ i ] = initno + addscheds.size();
						addscheds.add( tt.name() + i );
					}
				}
				task_assignment.put( tt, s );
			}
		}

		schedulers = new REFITSchedulerConfig[ initno + addscheds.size() ];

		for( int i = 0; i < initno; i++ )
		{
			String name = props.getProperty( "schedulers." + group + "." + i, defname + i );
			int[]  aff  = REFITConfig.getIntArrayProp( props,
					"schedulers." + group + "." + i + ".affinity", scheduler_affinity );

			schedulers[ i ] = new REFITSchedulerConfig( name, aff );
		}

		int i = initno;
		for( String n : addscheds )
			schedulers[ i++ ] = new REFITSchedulerConfig( n, scheduler_affinity );
	}


	public static final	REFITSchedulingConfig load(Properties props, String group, String defname,
			Collection<REFITSchedulerTaskType> tasks)
	{
		return new REFITSchedulingConfig( props, group, defname, tasks );
	}


	public static final int getNumberOfTasks(REFITSchedulerTaskType tt)
	{
		switch( tt )
		{
		case REPLICA_NETWORK_ENDPOINT_WORKER:
			return (REFITConfig.TOTAL_NR_OF_REPLICAS - 1) * REFITConfig.TOTAL_NUMBER_OF_REPLICA_NETWORKS;
		case CLIENT_NETWORK_ENDPOINT_WORKER:
			return REFITConfig.TOTAL_NR_OF_CLIENTS;
		case ORDER_STAGE:
			return REFITConfig.ORDERSTAGE.getNumber();
		default:
			return 1;
		}
	}

	public REFITSchedulerConfig[] getSchedulers()
	{
		return schedulers;
	}

	public Map<REFITSchedulerTaskType, int[]> getTaskAssignment()
	{
		return task_assignment;
	}

	public Map<REFITSchedulerTaskType, int[]> getGroupAssignment()
	{
		return group_assignment;
	}

	public int[] getProcessAffinity()
	{
		return process_affinity;
	}

	public int[] getDefaultSchedulerAffinity()
	{
		return scheduler_affinity;
	}
}
