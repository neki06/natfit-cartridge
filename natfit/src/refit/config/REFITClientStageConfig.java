package refit.config;

import java.util.Properties;

public class REFITClientStageConfig extends REFITStageConfig
{
	private final int[][] map_to_orderstages;
	private final int[][] map_to_addrs;
	private final int[][] map_to_workers;

	private REFITClientStageConfig(Properties props, REFITOrderStageConfig osconf, int[] replicaaddrs)
	{
		super( props, "clint" );

		map_to_orderstages = REFITConfig.reverseIndex( osconf.getMapToClientStages(), getNumber() );
		map_to_addrs       = loadArrayMapping( props, "clint", "addrs", replicaaddrs );
		map_to_workers     = loadArrayMapping( props, "order", "worker", null );
	}

	public static REFITClientStageConfig load(Properties props, REFITOrderStageConfig osconf, int[] replicaaddrs)
	{
		return new REFITClientStageConfig( props, osconf, replicaaddrs );
	}

	public final int[] getOrderStageIDs(int clientid)
	{
		return map_to_orderstages[ clientid ];
	}

	public final int[] getAddresses(int clientid)
	{
		return map_to_addrs[ clientid ];
	}

	public final int[] getWorkerIDs(int clientid)
	{
		return map_to_workers[ clientid ];
	}
}
