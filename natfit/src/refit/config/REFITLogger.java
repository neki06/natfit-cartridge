package refit.config;


public class REFITLogger {

	private static final boolean DISABLE_LOG = false;
	
	public static final boolean LOG_EVENT         = DISABLE_LOG ? false : false;
	public static final boolean LOG_INFO          = DISABLE_LOG ? false : true;
	public static final boolean LOG_COMMUNICATION = DISABLE_LOG ? false : false;

	public static final boolean LOG_CLIENT        = DISABLE_LOG ? false : false;
	public static final boolean LOG_ORDER         = DISABLE_LOG ? false : false;
	public static final boolean LOG_EXECUTION     = DISABLE_LOG ? false : false;
	public static final boolean LOG_CHECKPOINT    = DISABLE_LOG ? false : false;
	public static final boolean LOG_UPDATE        = DISABLE_LOG ? false : false;

	
	private static void logNormal(String type, Object component, String message) {
		System.out.println(type + " " + component + ": " + message);
	}

	private static void logError(String type, Object component, String message) {
		System.err.println(type + " " + component + ": " + message);
	}


	// ###########
	// # GENERAL #
	// ###########

	public static void logDebug(Object component, String message) {
		logError("[DEBUG]", component, message);
	}

	public static void logWarning(Object component, String message) {
		logError("[WARNG]", component, message);
	}

	public static void logError(Object component, String message) {
		logError("[ERROR]", component, message);
	}

	
	// ############
	// # SPECIFIC #
	// ############

	public static void logEvent(Object component, String message) {
		if(LOG_EVENT) logNormal("[EVENT]", component, message);
	}

	public static void logCommunication(Object component, String message) {
		if(LOG_COMMUNICATION) logNormal("[CMNCT]", component, message);
	}
	
	public static void logClient(Object component, String message) {
		if(LOG_CLIENT) logNormal("[CLINT]", component, message);
	}

	public static void logOrder(Object component, String message) {
		if(LOG_ORDER) logNormal("[ORDER]", component, message);
	}

	public static void logExecution(Object component, String message) {
		if(LOG_EXECUTION) logNormal("[EXECT]", component, message);
	}
	
	public static void logCheckpoint(Object component, String message) {
		if(LOG_CHECKPOINT) logNormal("[CHKPT]", component, message);
	}

	public static void logUpdate(Object component, String message) {
		if(LOG_UPDATE) logNormal("[UPDTE]", component, message);
	}

	public static void logInfo(Object component, String message) {
		if(LOG_INFO) logNormal("[INFRM]", component, message);
	}
}
