package refit.application.counter;

import java.nio.ByteBuffer;


public class REFITCounterServer {

	private final int[] counters;       //-- One dedicated counter for each client
	private final int   clientidbase;   //-- Where does the range of client ids start?
//	private final int   clientcnt;      //-- Number of clients and thus the number of available counters
	private final int   partid;         //-- ID of this state partition
	private final int   partcnt;        //-- Number of state partitions
	private final int   chkptsize;      //-- Calculated size of each checkpoint
	private final int   replysize;      //-- Minimum size of each reply.
	
	
	public REFITCounterServer(int partid, int partcnt, int clientidbase, int clientcnt, int replysize) {
		this.partid       = partid;
		this.partcnt      = partcnt;
		this.clientidbase = clientidbase;
		this.replysize    = replysize;

		this.counters  = new int[ clientcnt ];
		this.chkptsize = ( (clientcnt - 1) / partcnt + ((clientcnt + clientidbase - 1) % partcnt >= partid ? 1 : 0) ) * (Integer.SIZE >> 3);
	}

	
	public void init() {
		// Do nothing
	}
	
	public byte[] processRequest(int senderid, byte[] request, long consid) {
		// Create reply
		short counterID = (short) (senderid - clientidbase);
		ByteBuffer result = ByteBuffer.allocate(Math.max(replysize, Integer.SIZE >> 3));
		result.putInt(++counters[counterID]);
		return result.array();
	}

	public void applyUpdate(byte[] update) {
		ByteBuffer updateBuffer = ByteBuffer.wrap(update);
		short counterID = updateBuffer.getShort();
		counters[counterID] = updateBuffer.getInt();
	}
	
	public byte[] createCheckpoint() {
		ByteBuffer checkpoint = ByteBuffer.allocate( chkptsize );
		for(int i = 0; i < counters.length; i++)
			if( partcnt == 1 || (i + clientidbase) % partcnt == partid )
				checkpoint.putInt(counters[i]);
		return checkpoint.array();
	}
	
	public void applyCheckpoint(byte[] checkpoint) {
		ByteBuffer checkpointBuffer = ByteBuffer.wrap(checkpoint);
		for(int i = 0; i < counters.length; i++)
			if( partcnt == 1 || (i + clientidbase) % partcnt == partid )
				counters[i] = checkpointBuffer.getInt();
	}

}
