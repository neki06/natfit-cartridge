package refit.message;

import java.nio.ByteBuffer;
import java.security.MessageDigest;

import refit.util.REFITPayload;


public class REFITReply extends REFITMessage {

	public static final int REPLY_HEADER_SIZE = ((Byte.SIZE + Short.SIZE + Short.SIZE) >> 3);
	
	public short contactReplicaID;
	public final boolean isFullReply;
	public final byte[] payload;

	public REFITReply(REFITUniqueID uid, short from, boolean isFullReply, byte[] payload) {
		this( uid, from, isFullReply, payload, true );
	}


	protected REFITReply(REFITUniqueID uid, short from, boolean isFullReply, byte[] payload, Boolean verified) {
		super(REFITMessageType.REPLY, uid, from, verified);
		this.isFullReply = isFullReply;
		this.payload = payload;
		this.sendHashReply = false;
	}

	public REFITReply(ByteBuffer buffer) {
		super(buffer);
		this.contactReplicaID = buffer.getShort();
		this.isFullReply = (buffer.get() == 1);
		this.payload = new byte[buffer.getShort()];
		buffer.get(payload);
		this.sendHashReply = false;
	}

	
	public void setContactReplicaID(short contactReplicaID) {
		this.contactReplicaID = contactReplicaID;
	}
	
	@Override
	protected void serialize(ByteBuffer buffer) {
		super.serialize(buffer);
		buffer.putShort(contactReplicaID);
		buffer.put((byte) (isFullReply ? 1 : 0));
		buffer.putShort((short) payload.length);
		buffer.put(payload, 0, payload.length);
	}
	
	@Override
	protected int getHeaderSize() {
		return (super.getHeaderSize() + REPLY_HEADER_SIZE);
	}
	
	@Override
	protected int calculatePayloadSize() {
		return (super.calculatePayloadSize() + REPLY_HEADER_SIZE + payload.length);
	}

	@Override
	public String toString() {
		return String.format("{%s|%d|%d|%d|%s|%d bytes}", type, from, uid.nodeID, uid.seqNr, isFullReply, payload.length);
	}

	
	// ###########
	// # HASHING #
	// ###########

	public transient boolean sendHashReply;
	private transient byte[] payloadHash;
	

	public void sendHashReply() {
		sendHashReply = true;
	}

	@Override
	protected byte[] calculateHash() {
		// Prepare message header
		ByteBuffer messageHeader = getBuffer();
		messageHeader.limit(getHeaderSize());

		// Make sure that payload hash has been calculated (must be ensured in advance)
		getPayloadHash();
		
		// Calculate hash
		MessageDigest digest = REFITPayload.getDigest();
		digest.update(messageHeader);
		digest.update(payloadHash);
		return digest.digest();
	}
	
	public byte[] getPayloadHash() {
		if(payloadHash == null) payloadHash = REFITPayload.createHash(payload);
		return payloadHash;
	}
	
}
