package refit.message;

import java.nio.ByteBuffer;

import refit.agreement.pbft.REFITPBFTMessages.REFITPBFTCommit;
import refit.agreement.pbft.REFITPBFTMessages.REFITPBFTNewView;
import refit.agreement.pbft.REFITPBFTMessages.REFITPBFTPrePrepare;
import refit.agreement.pbft.REFITPBFTMessages.REFITPBFTPrepare;
import refit.agreement.pbft.REFITPBFTMessages.REFITPBFTViewChange;
import refit.config.REFITLogger;
import refit.replica.checkpoint.REFITCheckpoint;


public enum REFITMessageType {

	// General types
	REQUEST,
	REPLY,
	UPDATE,
	CHECKPOINT,
	BATCH,
	BATCH_UPDATE,

	INSTRUCTION,      // Internal message
	OPERATION,        // Internal message
	ORDER,            // Abstract message type

	// PBFT types
	PBFT_PRE_PREPARE,
	PBFT_PREPARE,
	PBFT_COMMIT,
	PBFT_VIEW_CHANGE,
	PBFT_NEW_VIEW;


	public static REFITMessageType getType(byte magic) {
		return values()[magic];
	}

	public byte getMagic() {
		return (byte) ordinal();
	}

	public REFITMessage createMessage(ByteBuffer buffer) {
		switch(this) {
		case REQUEST:
			return new REFITRequest(buffer);
		case REPLY:
			return new REFITReply(buffer);
		case CHECKPOINT:
			return new REFITCheckpoint(buffer);
		case BATCH:
			return new REFITBatch(buffer);
		case PBFT_PRE_PREPARE:
			return new REFITPBFTPrePrepare(buffer);
		case PBFT_PREPARE:
			return new REFITPBFTPrepare(buffer);
		case PBFT_COMMIT:
			return new REFITPBFTCommit(buffer);
		case PBFT_VIEW_CHANGE:
			return new REFITPBFTViewChange(buffer);
		case PBFT_NEW_VIEW:
			return new REFITPBFTNewView(buffer);
		default:
			REFITLogger.logError(REFITMessageType.class.getSimpleName(), "cannot create a message object for type " + this);
			return null;
		}
	}

}
