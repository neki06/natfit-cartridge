package refit.message;

import refit.config.REFITConfig;
import refit.util.REFITMessageStatistics;


public class REFITMessageContext {

	public long agreementSeqNr;
	
	public REFITMessageStatistics statistics;
	
	
	public REFITMessageContext() {
		this.agreementSeqNr = -1L;
		this.statistics = REFITConfig.COLLECT_MESSAGE_STATISTICS ? new REFITMessageStatistics() : null;;
	}
	
}
