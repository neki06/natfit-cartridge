package refit.message;

import java.nio.ByteBuffer;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import refit.config.REFITConfig;
import refit.config.REFITLogger;


public class REFITMessageAuthentication {

	private final short id;
	private final MacGenerator gen;
	private SecretKey    key;


	private REFITMessageAuthentication(short id, String password) {
		this.id = id;

		if( REFITConfig.HMAC_DIGESTONLY )
			gen = new DigestMacGenerator();
		else
		{
			try
			{
		        PBEKeySpec       spec = new PBEKeySpec( password.toCharArray() );
		        SecretKeyFactory fac  = SecretKeyFactory.getInstance( REFITConfig.HMAC_KEYALGORTHM );

		        key = fac.generateSecret(spec);
		        gen = new CryptoMacGenerator( key );
			}
			catch( NoSuchAlgorithmException | InvalidKeySpecException e )
			{
				throw new IllegalStateException( e );
			}
		}
	}


	public void initKey(SecretKey key)
	{
		if( !REFITConfig.HMAC_DIGESTONLY )
		{
			this.key = key;
			((CryptoMacGenerator) gen).initKey( key );

		}

	}


	public SecretKey getKey()
	{
		return key;
	}


	public static REFITMessageAuthentication createForReplicaConnection(short id)
	{
		return new REFITMessageAuthentication( id, "secret" );
	}


	public static REFITMessageAuthentication createForClientConnection(short id, short client, short replica)
	{
		return new REFITMessageAuthentication( id, REFITConfig.HMAC_UNIFIEDKEYS ? "secret" : client + ":" + replica );
	}


	@Override
	public String toString() {
		return "MAUTH";
	}

	private interface MacGenerator
	{
		byte[] createMac(REFITMessage msg, short sender, short recipient);
	}


	private static class DigestMacGenerator implements MacGenerator
	{
		private final MessageDigest digests;

		public DigestMacGenerator()
		{
			try
			{
				digests = MessageDigest.getInstance( REFITConfig.HMAC_ALGORITHM );
			}
			catch( NoSuchAlgorithmException e )
			{
				throw new IllegalStateException( e );
			}
		}

		@Override
		public byte[] createMac(REFITMessage msg, short sender, short recipient)
		{
			digests.update(msg.getHash());
			digests.update( (byte)(sender) );
			digests.update( (byte)(sender>>8) );
			digests.update( (byte)(recipient) );
			digests.update( (byte)(recipient>>8) );
			return digests.digest();
		}
	}


	private static class CryptoMacGenerator implements MacGenerator
	{
		private final Mac mac;

		public CryptoMacGenerator(SecretKey key)
		{
			try
			{
				mac = Mac.getInstance( REFITConfig.HMAC_ALGORITHM );
				mac.init( key );
			}
			catch( NoSuchAlgorithmException | InvalidKeyException e )
			{
				throw new IllegalStateException( e );
			}
		}


		public void initKey(SecretKey key)
		{
			try
			{
				mac.init( key );
			}
			catch( InvalidKeyException e )
			{
				throw new IllegalStateException( e );
			}
		}

		@Override
		public byte[] createMac(REFITMessage msg, short sender, short recipient)
		{
			mac.update( msg.getBuffer().array(), 0, msg.getMessageSize() - msg.getPaddingSize() );
			return mac.doFinal();
		}
	}


	// ###############
	// # UNICAST MAC #
	// ###############

	public void appendUnicastMAC(short recipientID, REFITMessage message) {
		if(!REFITConfig.AUTHENTICATE_MESSAGES) return;

		// Calculate individual MAC
		byte[] individualMAC = gen.createMac( message, id, recipientID );

		// Append MAC
		ByteBuffer messageBuffer = message.getBuffer();
		messageBuffer.position(message.getMessageSize() - REFITConfig.HMAC_UNICAST_SIZE);
		messageBuffer.put(individualMAC);
	}

	public boolean verifyUnicastMAC(REFITMessage message) {
		if(!REFITConfig.AUTHENTICATE_MESSAGES) return true;
		if(message.isVerified()!=null) return message.isVerified();

		// Calculate individual MAC
		byte[] individualMAC = gen.createMac( message, message.from, id );

		// Compare MACs
		ByteBuffer messageBuffer = message.getBuffer();
		messageBuffer.position(message.getMessageSize() - REFITConfig.HMAC_UNICAST_SIZE);
		for(int i = 0; i < individualMAC.length; i++) {
			if(individualMAC[i] != messageBuffer.get()) {
				REFITLogger.logWarning(this, "message verification failed");
				message.markVerified( false );
				return false;
			}
		}
		message.markVerified( true );
		return true;
	}


	// #################
	// # MULTICAST MAC #
	// #################

	public void appendMulticastMAC(short recipientID, REFITMessage message) {
		if(!REFITConfig.AUTHENTICATE_MESSAGES) return;

		ByteBuffer messageBuffer = message.getBuffer();
		messageBuffer.position(message.getMessageSize() - REFITConfig.HMAC_REPLICACAST_SIZE + (recipientID * REFITConfig.HMAC_UNICAST_SIZE));

		byte[] individualMAC = gen.createMac( message, id, recipientID );
		messageBuffer.put(individualMAC);
	}


	public void appendMulticastMAC(REFITMessage message) {
		if(!REFITConfig.AUTHENTICATE_MESSAGES) return;

		// Prepare message buffer
		ByteBuffer messageBuffer = message.getBuffer();
		messageBuffer.position(message.getMessageSize() - REFITConfig.HMAC_REPLICACAST_SIZE);

		// Calculate and append individual MACs
		for(byte recipientID = 0; recipientID < REFITConfig.TOTAL_NR_OF_REPLICAS; recipientID++) {
			// Calculate individual MAC
			byte[] individualMAC = gen.createMac( message, id, recipientID );

			messageBuffer.put(individualMAC);
		}
	}

	public boolean verifyMulticastMAC(REFITMessage message) {
		if(!REFITConfig.AUTHENTICATE_MESSAGES) return true;
		if(message.isVerified()!=null) return message.isVerified();

		// Calculate individual MAC
		byte[] individualMAC = gen.createMac( message, message.from, id );


		// Compare MACs
		ByteBuffer messageBuffer = message.getBuffer();
		messageBuffer.position(message.getMessageSize() - REFITConfig.HMAC_REPLICACAST_SIZE + (id * REFITConfig.HMAC_UNICAST_SIZE));
		for(int i = 0; i < individualMAC.length; i++) {
			if(individualMAC[i] != messageBuffer.get()) {
				REFITLogger.logWarning(this, "message verification failed");
				message.markVerified( false );
				throw new IllegalStateException();
//				return false;
			}
		}
		message.markVerified( true );
		return true;
	}


	// ###########################
	// # PUBLIC-KEY CRYPTOGRAPHY #
	// ###########################

	public void appendSignature(REFITMessage message) {
		// TODO: Implement properly
		appendMulticastMAC(message);
	}

	public boolean verifySignature(REFITMessage message) {
		// TODO: Implement properly
		return verifyMulticastMAC(message);
	}

}
