package refit.message;

import java.nio.ByteBuffer;
import java.security.MessageDigest;

import refit.config.REFITConfig;
import refit.util.REFITPayload;


public class REFITRequest extends REFITMessage {

	public static final REFITRequest NO_OP = new REFITRequest(new REFITUniqueID((short) -1, -1L), (short) -1, new byte[0], (short) -1);
	public static final int REQUEST_HEADER_SIZE = ((Byte.SIZE + Short.SIZE + Short.SIZE) >> 3);

	public boolean isPanic;
	public final short replyReplicaID;
	public final byte[] payload;

	
	public REFITRequest(REFITUniqueID uid, short from, byte[] payload, short replyReplicaID) {
		this(REFITMessageType.REQUEST, uid, from, payload, replyReplicaID);
	}

	protected REFITRequest(REFITMessageType type, REFITUniqueID uid, short from, byte[] payload, short replyReplicaID) {
		super(type, uid, from);
		this.isPanic = false;
		this.replyReplicaID = replyReplicaID;
		this.payload = payload;
	}
	
	public REFITRequest(ByteBuffer buffer) {
		super(buffer);
		this.isPanic = (buffer.get() == 1);
		this.replyReplicaID = buffer.getShort();
		this.payload = new byte[buffer.getShort()];
		buffer.get(payload);
	}

	
	public void markPanic() {
		isPanic = true;
	}

	@Override
	protected byte[] calculateHash() {
		// Shortcut for no-ops
		if(this == NO_OP) return new byte[0];
		
		// Optimization for small messages
		if(payload.length <= REFITPayload.HASH_SIZE) return payload;
		
		// Prepare message header
		ByteBuffer messageHeader = getBuffer();
		messageHeader.limit(calculatePayloadSize());
		
		// Calculate hash
		MessageDigest digest = REFITPayload.getDigest();
		digest.update(messageHeader);
		return digest.digest();
	}
	
	@Override
	public boolean equals(Object object) {
		if(!super.equals(object)) return false;
		if(REFITConfig.ENABLE_DEBUG_CHECKS) {
			if(!(object instanceof REFITRequest)) return false;
		}
		REFITRequest other = (REFITRequest) object;
		if(payload.length != other.payload.length) return false;
		for(int i = 0; i < payload.length; i++) {
			if(payload[i] != other.payload[i]) return false;
		}
		return true;
	}
	
	@Override
	protected void serialize(ByteBuffer buffer) {
		super.serialize(buffer);
		buffer.put((byte) (isPanic ? 1 : 0));
		buffer.putShort(replyReplicaID);
		buffer.putShort((short) payload.length);
		buffer.put(payload);
	}
	
	@Override
	protected int getHeaderSize() {
		return (super.getHeaderSize() + REQUEST_HEADER_SIZE);
	}
	
	@Override
	protected int calculatePayloadSize() {
		return (super.calculatePayloadSize() + REQUEST_HEADER_SIZE + payload.length);
	}

}
