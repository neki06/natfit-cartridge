package refit.message;

import javax.crypto.SecretKey;

import refit.message.REFITInstruction.REFITWorkNotification;
import refit.replica.REFITStage;

public class REFITMulticatMACVerificationInstruction extends REFITWorkNotification
{
	private final REFITMessage msg;
	private final REFITStage   stage;
	private final SecretKey    key;

	public REFITMulticatMACVerificationInstruction(REFITMessage msg, REFITStage stage, SecretKey key)
	{
		this.msg   = msg;
		this.stage = stage;
		this.key   = key;
	}


	@Override
	public void execute(REFITMessageAuthentication auth)
	{
		auth.initKey( key );
		auth.verifyMulticastMAC( msg );
		stage.insertMessage( msg );
	}

}
