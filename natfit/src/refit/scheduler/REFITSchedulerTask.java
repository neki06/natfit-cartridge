package refit.scheduler;

import java.io.IOException;
import java.nio.channels.SelectableChannel;
import java.nio.channels.SelectionKey;
import java.util.HashSet;
import java.util.Set;


public abstract class REFITSchedulerTask {

	public final REFITSchedulerTaskType taskType;
	public short group;
	public short id;
	
	protected Set<SelectionKey> readyKeys;
	private REFITScheduler scheduler;
	
	
	public REFITSchedulerTask(REFITSchedulerTaskType taskType, short group) {
		this.taskType = taskType;
		this.group = group;
		this.id = -1;
		this.throughput = 0L;
		REFITScheduler.registerTask(this);
	}
	
	
	// ################
	// # TASK METHODS #
	// ################

	public void initContext() {
		this.readyKeys = new HashSet<SelectionKey>();
	}
	
	public void setID(short id) {
		this.id = id;
	}
	
	public void setScheduler(REFITScheduler scheduler) {
		this.scheduler = scheduler;
	}
	
	public SelectionKey registerIO(SelectableChannel channel, int operations) throws IOException {
		return scheduler.registerIOTask(this, channel, operations);
	}
	
	public void selectKey(SelectionKey key) {
		readyKeys.add(key);
	}
	
	protected void progress() {
		if(scheduler != null) scheduler.notifyProgress(this);
	}
	
	public void work() {
		execute();
		readyKeys.clear();
	}
	
	public abstract void execute();

	
	// ##############
	// # STATISTICS #
	// ##############

	private long throughput;

	
	protected void event() {
		throughput++;
	}
	
	public long collectThroughput() {
		long tmp = throughput;
		throughput = 0L;
		return tmp;
	}

}
