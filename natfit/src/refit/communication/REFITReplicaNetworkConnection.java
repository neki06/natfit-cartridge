package refit.communication;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import refit.config.REFITConfig;
import refit.message.REFITMessage;
import refit.scheduler.REFITSchedulerTask;


public class REFITReplicaNetworkConnection {

	private static final boolean USE_STATIC_SEND_BUFFER = true;

	private SocketChannel socketChannel;
	private SelectionKey selectionKey;

	
	public REFITReplicaNetworkConnection(SocketChannel socketChannel, int receiveBufferSize, int sendBufferSize) {
		// Initialize attributes
		this.socketChannel = socketChannel;
		this.receiveQueue = new LinkedList<ByteBuffer>();
		this.receiveBuffer = ByteBuffer.allocateDirect(receiveBufferSize);
		this.receiveProcessedMark = 0;
		this.sendQueue = Collections.synchronizedList(new LinkedList<ByteBuffer>());
		this.staticSendBuffer = USE_STATIC_SEND_BUFFER ? ByteBuffer.allocateDirect(sendBufferSize) : null;
		this.sendBuffer = null;
		this.sendPending = false;

		try {
			// Configure channel
			socketChannel.configureBlocking(false);
			socketChannel.socket().setTcpNoDelay(REFITConfig.TCP_NO_DELAY);
		} catch(IOException ioe) {
			ioe.printStackTrace();
			closeConnection();
		}
	}
	
	
	@Override
	public String toString() {
		return "RNCTN";
	}
	
	public void register(REFITSchedulerTask task) {
		try {
			// Register channel for I/O operations
			selectionKey = task.registerIO(socketChannel, SelectionKey.OP_READ);
		} catch(IOException ioe) {
			closeConnection();
		}
	}

	public boolean isOpen() {
		return (socketChannel != null);
	}

	private void closeConnection() {
		if(selectionKey != null) selectionKey.cancel();
		try { socketChannel.close(); } catch(IOException ioe) { };
		socketChannel = null;
		selectionKey = null;
	}
	
	
	// ###########
	// # SENDING #
	// ###########

	private final ByteBuffer staticSendBuffer;
	private final List<ByteBuffer> sendQueue;
	private ByteBuffer sendBuffer;
	private boolean sendPending;

	
	public void enqueue(ByteBuffer message) {
		if(!isOpen()) return;
		sendQueue.add(message);
	}
	
	// TODO: Single send operation for retry and new data when using a static send buffer
	public void send() {
		// Check whether connection is open
		if(!isOpen()) return;

		// Try to complete pending send
		if(sendPending) {
			if(!selectionKey.isWritable()) return;
			boolean success = trySend();
			if(!success) return;
		}

		// Prepare send buffer
		while(!sendQueue.isEmpty()) {
			synchronized(sendQueue) {
				if(USE_STATIC_SEND_BUFFER && (sendQueue.size() > 1)) {
					sendBuffer = staticSendBuffer;
					for(Iterator<ByteBuffer> iterator = sendQueue.iterator(); iterator.hasNext(); ) {
						ByteBuffer message = iterator.next();
						if(sendBuffer.remaining() < message.remaining()) break;
						sendBuffer.put(message);
						iterator.remove();
					}
					sendBuffer.flip();
				} else {
					sendBuffer = sendQueue.remove(0);
				}
			}
			
			// Try to send data
			boolean success = trySend();
			if(!success) return;
		}
	}

	private boolean trySend() {
		try {
			// Try to send data
			int bytesToSend = sendBuffer.remaining();
			int bytesSent = socketChannel.write(sendBuffer);
			sendPending = (bytesSent < bytesToSend);
			if(sendPending) {
				//REFITLogger.logWarning(this, "write stalled (" + socketChannel.socket().getRemoteSocketAddress() + "): only " + bytesSent + " of " + bytesToSend + " bytes sent");
				selectionKey.interestOps(selectionKey.interestOps() | SelectionKey.OP_WRITE);
				return false;
			}
			
			// Sending has been successful
			if(sendBuffer == staticSendBuffer) sendBuffer.clear();
			else sendBuffer = null;
			selectionKey.interestOps(selectionKey.interestOps() & ~SelectionKey.OP_WRITE);
			return true;
		} catch(IOException ioe) {
			System.err.println(ioe);
			closeConnection();
			return false;
		}
	}

	
	// #############
	// # RECEIVING #
	// #############

	private final List<ByteBuffer> receiveQueue;
	private final ByteBuffer receiveBuffer;
	private int receiveProcessedMark;


	public List<ByteBuffer> receive() {
		// Check whether connection is open and data is available
		if(!isOpen()) return receiveQueue;
		if(!selectionKey.isReadable()) return receiveQueue;

		// Compact buffer if it is almost full
		if(receiveBuffer.position() > (receiveBuffer.capacity() - 1024)) {
			receiveBuffer.limit(receiveBuffer.position());
			receiveBuffer.position(receiveProcessedMark);
			receiveProcessedMark = 0;
			receiveBuffer.compact();
		}

		// Get data
		int bytesRead;
		try {
			// Receive new data
			bytesRead = socketChannel.read(receiveBuffer);
		} catch(IOException ioe) {
			bytesRead = -1;
		}

		// Close connection in case of an exception or EOF
		if(bytesRead < 0) {
			closeConnection();
			return receiveQueue;
		}
		
		// Prepare buffer for unmarshalling
		int oldPosition = receiveBuffer.position();
		receiveBuffer.limit(oldPosition);
		receiveBuffer.position(receiveProcessedMark);
		
		// Unmarshal messages
		while(receiveBuffer.remaining() > REFITMessage.HEADER_SIZE) {
			// Check for message size field
			int messageSize = receiveBuffer.getInt(receiveBuffer.position());
			if(receiveBuffer.remaining() < messageSize) break;
			
			// New complete message
			receiveProcessedMark = receiveBuffer.position() + messageSize;
			receiveBuffer.limit(receiveProcessedMark);
			// The buffer is stored at the message object to keep the serialized form, for instance,
			// to hash the message later if it becomes necessary. If only a slice is used and if
			// the receiveBuffer is too small, there is a chance that this slice of the
			// receiveBuffer has been already overridden when the hash is to be created.
//			ByteBuffer message = receiveBuffer.slice();
			ByteBuffer message = ByteBuffer.allocate( messageSize );
			message.put( receiveBuffer );
			message.rewind();

			receiveQueue.add(message);
			
			// Move processed mark to next message
			receiveBuffer.limit(oldPosition);
			receiveBuffer.position(receiveProcessedMark);
		}

		// Reset buffer position and limit for receiving
		receiveBuffer.position(oldPosition);
		receiveBuffer.limit(receiveBuffer.capacity());
		return receiveQueue;
	}

}
