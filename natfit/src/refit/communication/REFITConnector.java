package refit.communication;

import java.io.IOException;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

import refit.config.REFITConfig;
import refit.config.REFITLogger;


public class REFITConnector {

	public short remoteID  = -1;
	public short remoteNet = -1;
	public SocketChannel socketChannel;

	
	// #######################
	// # CONNECTION CREATION #
	// #######################

	public static REFITConnector connect(short net, SocketAddress localAddress, SocketAddress remoteAddress) {
		// Create connector object
		REFITConnector connector = new REFITConnector();
		connector.remoteID = REFITConfig.getNodeByAddress(remoteAddress);
		
		// Try to establish connection
		SocketChannel channel = null;
		int nrOfTries = 0;
		do {
			try {
				channel = SocketChannel.open(remoteAddress);
			} catch(IOException ioe) {
				if((++nrOfTries % 10) == 0) REFITLogger.logWarning(connector, ioe + " [" + remoteAddress + "]");
				try { Thread.sleep(100); } catch (InterruptedException e) {}
			}
		} while(channel == null);
		connector.socketChannel = channel;

		if( REFITConfig.CARRYOUT_HANDSHAKE )
		{
			try {
				// Send local node id
				ByteBuffer idBuffer = ByteBuffer.allocate(Short.SIZE >> 2);
				idBuffer.putShort(REFITConfig.getNodeByAddress(localAddress));
				idBuffer.putShort(net);
				idBuffer.rewind();
				channel.write(idBuffer);
			} catch(Exception e) {
				e.printStackTrace();
			}

			// Wait until other side has finished accept
			ByteBuffer readyBuffer = ByteBuffer.allocate(Byte.SIZE >> 3);
			while(true) {
				try { channel.read(readyBuffer); } catch(IOException ioe) { REFITLogger.logError(connector, REFITConfig.getNodeByAddress(localAddress) + ": " + ioe); }
				if(!readyBuffer.hasRemaining()) break;
				try { Thread.sleep(300); } catch (InterruptedException e) { }
			}
		}
		
		return connector;
	}

	public static REFITConnector accept(ServerSocketChannel serverSocketChannel) {
		try {
			// Accept connection
			SocketChannel channel = serverSocketChannel.accept();
			if(channel == null) return null;
			if(REFITLogger.LOG_COMMUNICATION) REFITLogger.logCommunication("SOCKET", "accepted connection " + channel);
			
			// Create connection object
			REFITConnector connector = new REFITConnector();
			connector.socketChannel = channel;
			
			if( REFITConfig.CARRYOUT_HANDSHAKE )
			{
				// Read remote id
				ByteBuffer idBuffer = ByteBuffer.allocate(Short.SIZE >> 2);
				do { channel.read(idBuffer); } while(idBuffer.hasRemaining());
				idBuffer.rewind();
				connector.remoteID  = idBuffer.getShort();
				connector.remoteNet = idBuffer.getShort();
			}

			if(REFITLogger.LOG_COMMUNICATION) REFITLogger.logCommunication(connector, "accepted connection to " + REFITConfig.ADDRESSES[connector.remoteID] + " net " + connector.remoteNet );
			return connector;
		} catch(IOException ioe) {
			ioe.printStackTrace();
			return null;
		}
	}

	public static void finishAccept(REFITConnector connector) {
		if( REFITConfig.CARRYOUT_HANDSHAKE )
		{
			// Tell other side that the connection has been completely accepted
			ByteBuffer readyBuffer = ByteBuffer.wrap(new byte[1]);
			try {
				int x = connector.socketChannel.write(readyBuffer);
				if(x != 1) REFITLogger.logError(connector, "accept finish failed (" + x + ")");
			} catch(IOException ioe) {
				ioe.printStackTrace();
			}
		}
	}

}
