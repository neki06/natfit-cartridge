package refit.communication;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import refit.config.REFITConfig;
import refit.scheduler.REFITSchedulerTask;


public class REFITConnection {

	private final REFITMessageFormat msgformat;

	private SocketChannel socketChannel;
	public  SelectionKey selectionKey;

	
	public REFITConnection(int receiveBufferSize, int sendBufferSize, REFITMessageFormat msgformat) {
		this.msgformat = msgformat;
		this.receiveQueue = new LinkedList<ByteBuffer>();
		this.receiveBuffer = ByteBuffer.allocateDirect(receiveBufferSize);
		this.sendQueue = Collections.synchronizedList(new LinkedList<ByteBuffer>());
		this.useStaticSendBuffer = (sendBufferSize > 0);
		this.staticSendBuffer = useStaticSendBuffer ? ByteBuffer.allocateDirect(sendBufferSize) : null;
		this.sendBuffer = null;
	}
	
	
	public void init(SocketChannel socketChannel) {
		// Reset helper data structures
		this.socketChannel = socketChannel;
		receiveBuffer.clear();
		receiveProcessedMark = 0;
		sendPending = false;
		
		try {
			// Configure channel
			socketChannel.configureBlocking(false);
			socketChannel.socket().setTcpNoDelay(REFITConfig.TCP_NO_DELAY);
		} catch(IOException ioe) {
			closeConnection();
		}
	}
	
	public void register(REFITSchedulerTask task) {
		try {
			// Register channel for I/O operations
			selectionKey = task.registerIO(socketChannel, SelectionKey.OP_READ);
		} catch(IOException ioe) {
			closeConnection();
		}
	}

	@Override
	public String toString() {
		return "CNCTN[" + socketChannel.socket() + "]";
	}
	
	public boolean isOpen() {
		return (socketChannel != null);
	}

	private void closeConnection() {
		if(selectionKey != null) selectionKey.cancel();
		try { socketChannel.close(); } catch(IOException ioe) { };
		socketChannel = null;
		selectionKey = null;
	}
	
	
	// ###########
	// # SENDING #
	// ###########

	private final ByteBuffer staticSendBuffer;
	private final List<ByteBuffer> sendQueue;
	private final boolean useStaticSendBuffer;
	private ByteBuffer sendBuffer;
	private boolean sendPending;

	
	public void enqueue(ByteBuffer message) {
		sendQueue.add(message);
	}
	
	public void send() {
		// Check whether connection is open
		if(!isOpen()) return;

		// Try to complete pending send
		if(sendPending) {
			if(!selectionKey.isWritable()) return;
			boolean success = trySend();
			if(!success) return;
		}

		// Prepare send buffer
		while(!sendQueue.isEmpty()) {
			synchronized(sendQueue) {
				if(useStaticSendBuffer && (sendQueue.size() > 1)) {
					sendBuffer = staticSendBuffer;
					for(Iterator<ByteBuffer> iterator = sendQueue.iterator(); iterator.hasNext(); ) {
						ByteBuffer message = iterator.next();
						if( msgformat.tryWriteMessage( sendBuffer, message ) )
							iterator.remove();
						else
							break;
					}
					sendBuffer.flip();
				} else {
					sendBuffer = msgformat.getSendBuffer( sendQueue.remove( 0 ) );
				}
			}
			
			// Try to send data
			boolean success = trySend();
			if(!success) return;
		}
	}

	private boolean trySend() {
		try {
			// Try to send data
			int bytesToSend = sendBuffer.remaining();
			int bytesSent = socketChannel.write(sendBuffer);
			sendPending = (bytesSent < bytesToSend);
			if(sendPending) {
				//REFITLogger.logWarning(this, "write stalled (" + socketChannel.socket().getRemoteSocketAddress() + ")");
				selectionKey.interestOps(selectionKey.interestOps() | SelectionKey.OP_WRITE);
				return false;
			}
			
			// Sending has been successful
			if(sendBuffer == staticSendBuffer) sendBuffer.clear();
			else sendBuffer = null;
			selectionKey.interestOps(selectionKey.interestOps() & ~SelectionKey.OP_WRITE);
			return true;
		} catch(IOException ioe) {
			System.err.println(ioe);
			closeConnection();
			return false;
		}
	}

	
	// #############
	// # RECEIVING #
	// #############

	private final List<ByteBuffer> receiveQueue;
	private final ByteBuffer receiveBuffer;
	private int receiveProcessedMark;


	public List<ByteBuffer> receive() {
		// Check whether connection is open and data is available
		if(!isOpen()) return receiveQueue;
		if(!selectionKey.isReadable()) return receiveQueue;

		// Compact buffer if it is almost full
		if(receiveBuffer.position() > ((receiveBuffer.capacity() * 3) >> 2)) {
			receiveBuffer.limit(receiveBuffer.position());
			receiveBuffer.position(receiveProcessedMark);
			receiveProcessedMark = 0;
			receiveBuffer.compact();
		}

		// Get data
		int bytesRead;
		try {
			// Receive new data
			bytesRead = socketChannel.read(receiveBuffer);
		} catch(IOException ioe) {
			bytesRead = -1;
		}
		
		// Close connection in case of an exception or EOF
		if(bytesRead < 0) {
			closeConnection();
			return receiveQueue;
		}
		
		// Prepare buffer for unmarshalling
		int oldPosition = receiveBuffer.position();
		receiveBuffer.limit(oldPosition);
		receiveBuffer.position(receiveProcessedMark);
		
		// Unmarshal messages
		while(receiveBuffer.hasRemaining()) {
			ByteBuffer msgbuf = msgformat.tryReadMessage(receiveBuffer);
			if(msgbuf == null) break;
			receiveQueue.add(msgbuf);
		}
		
		// Update processed mark and reset buffer
		receiveProcessedMark = receiveBuffer.position();
		receiveBuffer.limit(receiveBuffer.capacity());
		receiveBuffer.position(oldPosition);
		return receiveQueue;
	}

}
