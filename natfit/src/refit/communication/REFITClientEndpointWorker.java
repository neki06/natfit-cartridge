package refit.communication;

import java.util.List;

import refit.client.REFITBenchmarkClientLibrary;
import refit.message.REFITMessage;
import refit.scheduler.REFITScheduler;
import refit.scheduler.REFITSchedulerTask;


public class REFITClientEndpointWorker extends REFITSchedulerTask {

	private final REFITClientEndpoint endpoint;
	private REFITBenchmarkClientLibrary library;
	
	
	public REFITClientEndpointWorker(short id, REFITMessageFormat msgformat) {
		super(null, (short) 0);
		this.endpoint = new REFITClientEndpoint(id, msgformat);
	}

	
	@Override
	public String toString() {
		return endpoint.toString();
	}

	public void init() {
		endpoint.init();
	}

	public void setLibrary(REFITBenchmarkClientLibrary library) {
		this.library = library;
	}
	
	
	// ##################
	// # SCHEDULER TASK #
	// ##################

	@Override
	public void setScheduler(REFITScheduler scheduler) {
		super.setScheduler(scheduler);
		endpoint.register(this);
	}

	@Override
	public void execute() {
		// Handle sending
		endpoint.executeSends();

		// Receive messages
		if(!readyKeys.isEmpty()) {
			List<REFITMessage> messages = endpoint.executeReceives(readyKeys);
			for(REFITMessage message: messages) library.insertMessage(message);
		}
	}

	
	// ####################
	// # SENDING MESSAGES #
	// ####################

	public void sendToReplica(REFITMessage message, short replicaID) {
		endpoint.sendToReplica(message, replicaID);
		progress();
	}
	
	public void sendToAllReplicas(REFITMessage message) {
		endpoint.sendToAllReplicas(message);
		progress();
	}
	
}
