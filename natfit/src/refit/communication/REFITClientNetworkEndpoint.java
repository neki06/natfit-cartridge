package refit.communication;

import java.io.IOException;
import java.nio.channels.SelectionKey;
import java.nio.channels.ServerSocketChannel;

import refit.config.REFITConfig;
import refit.config.REFITLogger;
import refit.message.REFITReply;
import refit.message.REFITRequest;
import refit.replica.REFITReplica;
import refit.scheduler.REFITScheduler;
import refit.scheduler.REFITSchedulerTask;
import refit.scheduler.REFITSchedulerTaskType;


public class REFITClientNetworkEndpoint extends REFITSchedulerTask {

	private final REFITReplica replica;
	private final REFITClientNetworkEndpointWorker[] workers;

	private ServerSocketChannel[] serverSocketChannels;
	
	
	public REFITClientNetworkEndpoint(REFITReplica replica) {
		super(REFITSchedulerTaskType.CLIENT_NETWORK_ENDPOINT, (short) 0);
		this.replica = replica;
		this.workers = new REFITClientNetworkEndpointWorker[REFITConfig.TOTAL_NR_OF_CLIENTS];
	}
	
	
	@Override
	public String toString() {
		return "CLTEP";
	}
	
	public void init() {
		// Create connection workers
		for(short i = 0; i < workers.length; i++) {
			workers[i] = new REFITClientNetworkEndpointWorker(this, (short) (REFITConfig.TOTAL_NR_OF_REPLICAS + i),
					(short) (i % REFITConfig.CLIENTSTAGE.getNumber()) );
		}
		
		// Create server socket
		serverSocketChannels = new ServerSocketChannel[ REFITConfig.NUMBER_OF_REPLICA_ADDRESSES ];

		for( int i = 0; i < REFITConfig.NUMBER_OF_REPLICA_ADDRESSES; i++ )
		{
			try {
				serverSocketChannels[i] = ServerSocketChannel.open();
				serverSocketChannels[i].configureBlocking(false);
				serverSocketChannels[i].socket().bind(REFITConfig.ADDRESSES[replica.id][i]);
			} catch(IOException ioe) {
				ioe.printStackTrace();
			}
		}
	}
	

	// ##################
	// # SCHEDULER TASK #
	// ##################

	@Override
	public void setScheduler(REFITScheduler scheduler) {
		super.setScheduler(scheduler);
		try {
			for( ServerSocketChannel serverSocketChannel : serverSocketChannels )
				registerIO(serverSocketChannel, SelectionKey.OP_ACCEPT);
		} catch(IOException ioe) {
			ioe.printStackTrace();
		}
	}

	@Override
	public void execute() {
		for( ServerSocketChannel serverSocketChannel : serverSocketChannels )
		{
			while(true) {
				// Accept new connection
				REFITConnector connector = REFITConnector.accept(serverSocketChannel);
				if(connector == null) break;
				REFITConnector.finishAccept(connector);
				if(REFITLogger.LOG_COMMUNICATION) REFITLogger.logCommunication("SOCKET", "accepted connection " + connector.socketChannel.socket());

				// Hand connection over to worker
				workers[connector.remoteID - REFITConfig.TOTAL_NR_OF_REPLICAS].init(connector.socketChannel);
				event();
			}
		}
	}

	
	// ##################
	// # HELPER METHODS #
	// ##################

	public void sendReply(REFITReply reply, short clientID) {
		workers[clientID - REFITConfig.TOTAL_NR_OF_REPLICAS].sendReply(reply);
	}

	public void handleRequest(REFITRequest request) {
		replica.receiveMessageFromClient(request);
	}
	
}
