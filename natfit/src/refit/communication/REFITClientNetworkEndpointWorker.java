package refit.communication;

import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.List;

import refit.config.REFITConfig;
import refit.message.REFITReply;
import refit.message.REFITRequest;
import refit.scheduler.REFITSchedulerTask;
import refit.scheduler.REFITSchedulerTaskType;
import refit.util.REFITMessageStatistics;


public class REFITClientNetworkEndpointWorker extends REFITSchedulerTask {

	private final REFITClientNetworkEndpoint endpoint;
	private final short id;
	private final REFITConnection connection;
	private boolean initCalled;


	public REFITClientNetworkEndpointWorker(REFITClientNetworkEndpoint endpoint, short id, short group) {
//		super((id % 3) == 0 ? REFITSchedulerTaskType.CLIENT_NETWORK_ENDPOINT_WORKER : (id % 3) == 1 ? REFITSchedulerTaskType.CLIENT_NETWORK_ENDPOINT_WORKER_2 : REFITSchedulerTaskType.CLIENT_NETWORK_ENDPOINT_WORKER_3);
		super(REFITSchedulerTaskType.CLIENT_NETWORK_ENDPOINT_WORKER, group);
		this.endpoint = endpoint;
		this.id = id;
		this.connection = new REFITConnection(REFITConfig.CLIENT_NETWORK_RECEIVE_BUFFER_SIZE, 0, new REFITMessageFormat());
		this.initCalled = false;
	}

	
	@Override
	public String toString() {
		return String.format("CLW%02d%03d", group, id);
	}

	public void init(SocketChannel socketChannel) {
		connection.init(socketChannel);
		initCalled = true;
		progress();
	}
	
	
	// ##################
	// # SCHEDULER TASK #
	// ##################

	@Override
	public void execute() {
		// TODO: Apparently register() must be called by the scheduler that owns the selector
		if(initCalled) {
			connection.register(this);
			initCalled = false;
		}
		
		// Handle sending
		connection.send();
		
		// Return if there is nothing to be received
		if(!readyKeys.contains(connection.selectionKey)) return;

		// Handle receiving
		List<ByteBuffer> messages = connection.receive();
		for(ByteBuffer message: messages) {
			// Create request
			REFITRequest request = new REFITRequest(message);
			if(REFITConfig.COLLECT_MESSAGE_STATISTICS) REFITMessageStatistics.track(request, "REC");
			request.serializeMessage(0);
	
			// Forward request
			endpoint.handleRequest(request);
			event();
		}
		messages.clear();
	}


	// ##################
	// # REPLY HANDLING #
	// ##################

	public void sendReply(REFITReply reply) {
		connection.enqueue(reply.getBuffer());
		progress();
	}

}
