package refit.communication;

import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.List;

import refit.config.REFITConfig;
import refit.config.REFITLogger;
import refit.message.REFITMessage;
import refit.replica.REFITReplica;
import refit.scheduler.REFITScheduler;
import refit.scheduler.REFITSchedulerTask;
import refit.scheduler.REFITSchedulerTaskType;


public class REFITReplicaNetworkEndpointWorker extends REFITSchedulerTask {

	private final REFITReplica replica;
	private final short        id;
	private REFITReplicaNetworkConnection connection;
	private final SocketChannel socketChannel;


	public REFITReplicaNetworkEndpointWorker(REFITReplica replica, short group, short id, SocketChannel socketChannel) {
		super(REFITSchedulerTaskType.REPLICA_NETWORK_ENDPOINT_WORKER, group);
		this.replica = replica;
		this.id      = id;
		this.socketChannel = socketChannel;
	}

	
	@Override
	public String toString() {
		return String.format( "RW%02d%d", group, id );
	}

	
	// ##################
	// # SCHEDULER TASK #
	// ##################

	@Override
	public void initContext() {
		super.initContext();

		connection = new REFITReplicaNetworkConnection(socketChannel, REFITConfig.REPLICA_NETWORK_RECEIVE_BUFFER_SIZE, REFITConfig.REPLICA_NETWORK_SEND_BUFFER_SIZE);
		connection.register(this);
	}


	@Override
	public void setScheduler(REFITScheduler scheduler) {
		super.setScheduler(scheduler);
	}
	
	@Override
	public void execute() {
		// Handle receiving
		List<ByteBuffer> messages = connection.receive();
		for(ByteBuffer message: messages) {
			// Create message
			REFITMessage msg = REFITMessage.createMessage(message);
			if(REFITLogger.LOG_COMMUNICATION) REFITLogger.logCommunication(this, "received message " + msg + " from " + msg.from + " (" + REFITConfig.ADDRESSES[msg.from] + ")");
	
			// Forward message
			replica.receiveMessageFromReplica(msg);
			event();
		}
		messages.clear();

		// Handle sending
		connection.send();
	}


	// ###########
	// # SENDING #
	// ###########

	public void enqueueMessage(ByteBuffer message) {
		connection.enqueue(message);
		progress();
	}

}
