package refit.client;

import java.lang.reflect.Constructor;

import refit.communication.REFITClientEndpointWorker;
import refit.config.REFITConfig;
import refit.config.REFITLogger;
import refit.config.REFITSchedulingConfig;
import refit.scheduler.REFITScheduler;
import refit.scheduler.REFITScheduling;


public class REFITBenchmark extends REFITBenchmarkBase {

	
	public REFITBenchmark(short id, long timelogresol, String resultpath)
	{
		super( id, timelogresol, resultpath );
	}
	
	
	@SuppressWarnings("unchecked")
	public void runBenchmark(long duration, int clientIDOffset, Class<?> benchmarkType, Class<? extends REFITBenchmarkClientLibrary> libraryType) {
		// Create schedulers
		REFITSchedulingConfig schedconf = REFITConfig.CLIENT_SCHEDULING;
		REFITScheduler[] schedulers = new REFITScheduler[schedconf.getSchedulers().length];
		for(int i = 0; i < schedulers.length; i++)
			schedulers[i] = new REFITScheduler( schedconf.getSchedulers()[i] );

		// Create communication endpoints
//		REFITScheduler endpointScheduler = new REFITScheduler();
		REFITBenchmarkClient[] clients = new REFITBenchmarkClient[REFITConfig.TOTAL_NR_OF_CLIENTS];

		for(short i = id; i < clients.length; i += REFITConfig.TOTAL_NR_OF_CLIENTHOSTS ) {
			try {
				// Create client library
				Constructor<? extends REFITBenchmarkClientLibrary> libraryConstructor = libraryType.getConstructor(new Class[] { short.class });
				REFITBenchmarkClientLibrary library = libraryConstructor.newInstance(new Object[] { (short) (clientIDOffset + i) });
				// Assign library to scheduler
				schedulers[i % schedulers.length].assignTask(library);
				
				REFITClientEndpointWorker endpoint = new REFITClientEndpointWorker((short) (clientIDOffset + i), library.getMessageFormat());
				endpoint.init();
				endpoint.setLibrary(library);
				library.init( endpoint );
//				endpointScheduler.assignTask(endpoints[i]);
				schedulers[i % schedulers.length].assignTask(endpoint);

				// Create client
				Constructor<? extends REFITBenchmarkClient> benchmarkConstructor = (Constructor<? extends REFITBenchmarkClient>) benchmarkType.getConstructor(new Class[] { short.class, REFITClientProxy.class });
				clients[i] = benchmarkConstructor.newInstance(new Object[] { (short) (clientIDOffset + i), library });
			} catch(Exception e) {
				e.printStackTrace();
				System.exit(1);
			}
		}
//		endpointScheduler.start();

		// Start schedulers
		for(REFITScheduler scheduler: schedulers) {
			scheduler.init();
			scheduler.start();
		}

		REFITConfig.printSchedulingConfig( schedconf, schedulers );

		runBenchmark( duration, clients );
	}

	
	// ##############
	// # STATISTICS #
	// ##############




	// ########
	// # MAIN #
	// ########

	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws Exception {
		if(args.length < 6) {
			System.err.println("usage: java " + REFITBenchmark.class.getSimpleName() + "<id> <config> <duration> <client-id-offset> <benchmark> <library>");
			System.exit(1);
		}

		short id = Short.parseShort( args[0] );

		REFITConfig.load( args[1] );

		if( REFITLogger.LOG_INFO )
		{
			REFITLogger.logInfo( "CONFIG", "ID: " + id );
			REFITLogger.logInfo( "CONFIG", "No of client hosts: " + REFITConfig.TOTAL_NR_OF_CLIENTHOSTS );
			REFITLogger.logInfo( "CONFIG", "Send buffer: " + REFITConfig.CLIENT_SEND_BUFFER_SIZE );
			REFITLogger.logInfo( "CONFIG", "Recv buffer: " + REFITConfig.CLIENT_RECEIVE_BUFFER_SIZE );
			REFITLogger.logInfo( "CONFIG", "Request size: " + REFITConfig.REQUEST_SIZE );
			REFITLogger.logInfo( "CONFIG", "Reply size  : " + REFITConfig.REPLY_SIZE );
		}


		if( REFITConfig.CLIENT_SCHEDULING.getProcessAffinity()!=null )
			REFITScheduling.setProcessAffinity( REFITConfig.CLIENT_SCHEDULING.getProcessAffinity() );

		long duration = Integer.parseInt(args[2]) * 1000;
		int clientIDOffset = REFITConfig.TOTAL_NR_OF_REPLICAS + Integer.parseInt(args[3]);
		Class<?> benchmarkType = Class.forName(args[4]);
		Class<? extends REFITBenchmarkClientLibrary> libraryType = (Class<? extends REFITBenchmarkClientLibrary>) Class.forName(args[5]);

		REFITBenchmark benchmark = new REFITBenchmark( id, REFITConfig.TIMELOG_RESOL, REFITConfig.RESULTS_PATH );
		benchmark.runBenchmark( duration, clientIDOffset, benchmarkType, libraryType);
	}

}
