package refit.client;

import java.util.List;

import refit.config.REFITConfig;
import refit.config.REFITLogger;
import refit.message.REFITMessageAuthentication;
import refit.message.REFITReply;
import refit.message.REFITUniqueID;
import refit.util.REFITBallotBox;
import refit.util.REFITPayload;


public class REFITReplyCertificate {

	private static final int STABILITY_THRESHOLD = REFITConfig.FAULTS_TO_TOLERATE + 1;

	public final REFITBallotBox<Short, REFITPayload, REFITReply> replies;
	private final REFITMessageAuthentication[] auths;
	private REFITUniqueID uid;
	public REFITReply result;
	
	
	public REFITReplyCertificate(REFITMessageAuthentication[] auths) {
		this.replies = new REFITBallotBox<Short, REFITPayload, REFITReply>(STABILITY_THRESHOLD);
		this.auths = auths;
	}
	
	
	public void init(REFITUniqueID uid) {
		replies.clear();
		this.uid = uid;
		result = null;
	}

	public boolean add(REFITReply reply) {
		// Check reply
		if(reply == null) return false;
		if(!uid.equals(reply.uid)) return false;
		if(!auths[reply.from].verifyUnicastMAC(reply)) return false;
		
		// Prepare vote
		REFITPayload vote;
		if(REFITConfig.USE_HASHED_REPLIES) {
			vote = new REFITPayload(reply.isFullReply ? reply.getPayloadHash() : reply.payload);
		} else {
			vote = new REFITPayload(reply.payload);
		}
		
		// Check whether the reply is a late full reply
		REFITPayload decision = replies.getDecision();
		if(reply.isFullReply && (decision != null) && vote.equals(decision)) {
			result = reply;
			return true;
		}
		
		// Cast vote
		boolean success = replies.add(reply.from, vote, reply);
		if(success) checkResult();
		return success;
	}

	public boolean isStable() {
		return (result != null);
	}

	private void checkResult() {
		if(isStable()) return;
		if(REFITConfig.ENABLE_DEBUG_CHECKS) if(replies.votesDiffer()) REFITLogger.logWarning("[REPLY]", "received different results for request " + uid);
		List<REFITReply> correctReplies = replies.getDecidingBallots();
		if(correctReplies == null) return;
		for(REFITReply reply: correctReplies) {
			if(!reply.isFullReply) continue;
			result = reply;
			break;
		}
	}
	
}
